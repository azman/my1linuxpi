/*----------------------------------------------------------------------------*/
#include "i2c.h"
#include "gpio.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define SDA_GPIO DEFAULT_SDA_GPIO
#define SCL_GPIO DEFAULT_SCL_GPIO
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	i2c_t that;
	unsigned char buff[8];
	int addr, test;
	printf("-- Initialize I2C\n");
	fflush(stdout);
	gpio_init();
	i2c_init(&that,SDA_GPIO,SCL_GPIO);
	printf("-- Scanning I2C Bus");
	for (addr=0;addr<0x80;addr++) {
		if (addr%8==0) printf("\n");
		printf("  0x%02x:",addr);
		if (!(test=i2c_gets(&that,addr,0x00,buff,1)))
			printf("%02x",buff[0]);
		else printf("--");
	}
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
