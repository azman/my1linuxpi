/*----------------------------------------------------------------------------*/
#include "gpio.h"
#include "timer.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1key_t key;
	int flag, gpio_num;
	char *pname = basename(argv[0]);
	/* if no param, show help and exit? */
	if (argc<2) {
		printf("\n%s - Raspberry Pi Basic GPIO Tool\n",pname);
		printf("Usage:\n");
		printf("    %s [gpio_num] <command>\n\n",pname);
		return 0;
	}
	/* initialize gpio library */
	if (gpio_init()==GPIO_STATUS_ERROR) {
		printf("Cannot initialize GPIO library! Aborting!\n");
		exit(1);
	}
	/* first param is always gpio number */
	gpio_num = atoi(argv[1]);
	if (gpio_num<2||gpio_num>27) {
		printf("\n[%s] Invalid GPIO number '%d'! (Valid range: 2-27)\n\n",
			pname,gpio_num);
		exit(1);
	}
	/* if no command, print stat and exit */
	if (argc<3) {
		printf("\n[%s] GPIO%d: ",pname,gpio_num);
		flag = gpio_check(gpio_num);
		switch (flag) {
			case GPIO_INPUT:
				printf("Input (Value: %d)",gpio_read(gpio_num)?1:0);
				break;
			case GPIO_OUTPUT:
				printf("Output (Value: %d)",gpio_read(gpio_num)?1:0);
				break;
			case GPIO_ALTF0: printf("Alt-F0"); break;
			case GPIO_ALTF1: printf("Alt-F1"); break;
			case GPIO_ALTF2: printf("Alt-F2"); break;
			case GPIO_ALTF3: printf("Alt-F3"); break;
			case GPIO_ALTF4: printf("Alt-F4"); break;
			case GPIO_ALTF5: printf("Alt-F5"); break;
			default: printf("Invalid!");
		}
		printf("\n\n");
		return 0;
	}
	/* check command */
	putchar('\n');
	if (!strncmp(argv[2],"in",3)||!strncmp(argv[2],"input",6)) {
		printf("[%s] Configure GPIO%d as input... ",pname,gpio_num);
		gpio_config(gpio_num,GPIO_INPUT);
		printf("done.\n");
	}
	else if (!strncmp(argv[2],"out",4)||!strncmp(argv[2],"output",7)) {
		printf("[%s] Configure GPIO%d as output... ",pname,gpio_num);
		gpio_config(gpio_num,GPIO_OUTPUT);
		printf("done.\n");
	}
	else if (!strncmp(argv[2],"done",5)||!strncmp(argv[2],"release",8)) {
		printf("[%s] GPIO%d released (DUMMY).\n",pname,gpio_num);
	}
	else if (!strncmp(argv[2],"high",5)||!strncmp(argv[2],"hi",4)||
			!strncmp(argv[2],"1",2)) {
		if (gpio_check(gpio_num)!=GPIO_OUTPUT) {
			printf("[%s] GPIO%d is NOT an output pin! Aborting!\n\n",
				pname,gpio_num);
			exit(1);
		}
		printf("[%s] Setting GPIO%d to high... ",pname,gpio_num);
		gpio_set(gpio_num);
		printf("done.\n");
	}
	else if (!strncmp(argv[2],"low",4)||!strncmp(argv[2],"lo",3)||
			!strncmp(argv[2],"0",2)) {
		if (gpio_check(gpio_num)!=GPIO_OUTPUT) {
			printf("[%s] GPIO%d is NOT an output pin! Aborting!\n\n",
				pname,gpio_num);
			exit(1);
		}
		printf("[%s] Setting GPIO%d to low... ",pname,gpio_num);
		gpio_clr(gpio_num);
		printf("done.\n");
	}
	else if (!strncmp(argv[2],"read",5)||!strncmp(argv[2],"value",6)) {
		if (gpio_check(gpio_num)!=GPIO_INPUT) {
			printf("[%s] GPIO%d is NOT an input pin! Aborting!\n\n",
				pname,gpio_num);
			exit(1);
		}
		printf("[%s] GPIO%d value: %d\n",pname,gpio_num,
			gpio_read(gpio_num)?1:0);
	}
	else if (!strncmp(argv[2],"clock",6)||!strncmp(argv[2],"squarewave",11)) {
		int period;
		if (gpio_check(gpio_num)!=GPIO_OUTPUT) {
			printf("[%s] GPIO%d is NOT an output pin! Aborting!\n\n",
				pname,gpio_num);
			exit(1);
		}
		if (argc<4) {
			printf("\n[%s] Missing parameter for squarewave!\n\n",pname);
			exit(1);
		}
		period = atoi(argv[3]);
		printf("[%s] Generating %.3fkHz squarewave @GPIO%d (ESC to stop)... ",
			pname,(float)(1000.0/period),gpio_num);
		while (1) {
			key = get_keyhit();
			if (key==KEY_ESCAPE) break;
			gpio_toggle(gpio_num);
			timer_wait(period/2);
		}
		printf("done.\n");
	}
	else printf("[%s] Unknown command '%s'!\n",pname,argv[2]);
	putchar('\n');
	return 0;
}
/*----------------------------------------------------------------------------*/
