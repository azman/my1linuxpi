/*----------------------------------------------------------------------------*/
#include "gpio.h"
#include "gsm.h"
#include "my1uart.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>
#include <unistd.h>
/*----------------------------------------------------------------------------*/
#define BUFF_SIZE 80
#define GSM_WAIT_RETRY 2
/*----------------------------------------------------------------------------*/
void do_debug(char* chkstr, char* buffer, int count)
{
	int loop;
	if (chkstr) printf(chkstr);
	printf("[DEBUG] {");
	for (loop=0;loop<count;loop++)
	{
		unsigned int byte = (unsigned int)(buffer[loop]);
		if (byte<0x20||byte>0x7f)
		{
			printf("[0x%02X]",byte);
		}
		else printf("%c",byte);
	}
	printf("}[DONE](0x%02X)\n",count);
}
/*----------------------------------------------------------------------------*/
void do_check(char* buffer, int count)
{
	int check = gsm_replies(buffer,count,0x0);
	if (check) do_debug(0x0,buffer,check);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	my1uart_t port;
	my1key_t key;
	char *pname = basename(argv[0]);
	char *phone, buffer[BUFF_SIZE], *pbuff;
	int do_call = 0, do_mesg = 0, count;
	unsigned int okchk;
	/* always show program name/info  */
	printf("\n%s - Raspberry Pi GSM Interface\n\n",pname);
	/* check param */
	if (argc!=3)
	{
		printf("Requires 2 parameters!\n\n");
		exit(1);
	}
	if (strncmp(argv[1],"call",4)==0) do_call = 1;
	else if (strncmp(argv[1],"sms",3)==0) do_mesg = 1;
	else
	{
		printf("Invalid command '%s'!\n\n",argv[1]);
		exit(1);
	}
	phone = argv[2];
	/* initialize gpio library */
	printf("Initializing GPIO library... ");
	if (gpio_init()==GPIO_STATUS_ERROR)
	{
		printf("Cannot initialize GPIO library! Aborting!\n\n");
		exit(1);
	}
	printf("done.\n");
	/* initialize serial port */
	printf("Initializing serial interface... ");
	uart_init(&port);
	sprintf(port.name,"/dev/ttyAMA"); /* default on raspbian? */
	/* try to prepare pi serial port */
	if(!uart_prep(&port,1))
	{
		printf("\n\nCannot prepare port '%s0'!\n\n",port.name);
		exit(1);
	}
	printf("done.\n");
	/* try opening port */
	printf("Accessing serial port... ");
	if(!uart_open(&port))
	{
		printf("\n\nCannot open port '%s%d'!\n\n",port.name,
			COM_PORT(port.term));
		exit(1);
	}
	printf("done.\n");
	/* clear input buffer */
	uart_purge(&port);
	/* initialize gsm module */
	gsm_init((void*)&port);
	/* looking for modem response */
	while(1)
	{
		printf("Sending 'AT'... "); fflush(stdout);
		gsm_command("AT");
		count = gsm_replies(buffer,BUFF_SIZE,&okchk);
		if (count==0)
		{
			printf("no response.\n");
		}
		else
		{
			if (okchk==GSM_OK_COMPLETE)
			{
				printf("OK.\n");
				break;
			}
			else
			{
				/** do_debug("invalid response.\n",buffer,count); */
				printf("invalid response.\n");
			}
		}
		sleep(GSM_WAIT_RETRY); /* wait between retries */
	}
	/* check if gsm module said something */
	do_check(buffer,BUFF_SIZE);
	/* try to disable echo - tc35 needs this */
	printf("Sending 'ATE'... ");
	gsm_command("ATE");
	count = gsm_replies(buffer,BUFF_SIZE,&okchk);
	if (count==0)
	{
		printf("no response.\n\n");
		exit(1);
	}
	else
	{
		if (okchk==GSM_OK_COMPLETE)
		{
			printf("OK.\n");
		}
		else
		{
			do_debug("invalid response.\n\n",buffer,count);
			exit(1);
		}
	}
	do_check(buffer,BUFF_SIZE);
	/* on sim908, must send ate 0 */
	printf("Sending 'ATE 0'... ");
	gsm_command("ATE 0");
	count = gsm_replies(buffer,BUFF_SIZE,&okchk);
	if (count==0)
	{
		printf("no response.\n\n");
		exit(1);
	}
	else
	{
		pbuff = gsm_trim_prefix(buffer);
		if (okchk==GSM_OK_COMPLETE)
		{
			printf("OK.\n");
		}
		else if (!strncmp(pbuff,"ERROR",5))
		{
			printf("ignored.\n");
		}
		else
		{
			do_debug("invalid response.\n\n",buffer,count);
			exit(1);
		}
	}
	do_check(buffer,BUFF_SIZE);
	/* check if sim card is in */
	printf("Checking SIM status... ");
	gsm_command("AT+CPIN?");
	count = gsm_replies(buffer,BUFF_SIZE,&okchk);
	if (count==0)
	{
		printf("no response.\n\n");
		exit(1);
	}
	else
	{
		pbuff = gsm_trim_prefix(buffer);
		if (!strncmp(pbuff,"+CPIN: READY",12))
		{
			printf("SIM ready.\n");
		}
		else if (!strncmp(pbuff,"ERROR",5))
		{
			printf("missing SIM?\n\n");
			exit(1);
		}
		else
		{
			do_debug("invalid response.\n\n",buffer,count);
			exit(1);
		}
	}
	do_check(buffer,BUFF_SIZE);
	/* setting text mode for sending sms */
	printf("Setting SMS Text Mode... ");
	gsm_command("AT+CMGF=1");
	count = gsm_replies(buffer,BUFF_SIZE,&okchk);
	if (count==0)
	{
		printf("no response.\n\n");
		exit(1);
	}
	else
	{
		if (okchk==GSM_OK_COMPLETE)
		{
			printf("OK.\n");
		}
		else
		{
			do_debug("invalid response.\n\n",buffer,count);
			exit(1);
		}
	}
	do_check(buffer,BUFF_SIZE);
	if (do_call)
	{
		/** make a call... */
		printf("Calling %s... ",phone);
		sprintf(buffer,"ATD%s;",phone);
		gsm_command(buffer);
		while(1)
		{
			key = get_keyhit();
			if(key!=KEY_NONE&&key==KEY_ESCAPE)
			{
				printf("\nHanging up... ");
				gsm_command("ATH");
				count = gsm_replies(buffer,BUFF_SIZE,&okchk);
				if (count)
				{
					pbuff = gsm_trim_prefix(buffer);
					if (!strncmp(pbuff,"NO CARRIER",10))
					{
						printf("done!\n");
						break;
					}
					else
					{
						do_debug("huh?\n",buffer,count);
					}
				}
				else
				{
					printf("\n");
				}
				break;
			}
			if (uart_incoming(&port))
			{
				count = gsm_replies(buffer,BUFF_SIZE,&okchk);
				if (count) do_debug(0x0,buffer,count);
				pbuff = gsm_trim_prefix(buffer);
				if (!strncmp(pbuff,"NO CARRIER",10))
				{
					printf("\nCall rejected?\n");
					break;
				}
			}
		}
	}
	else if (do_mesg)
	{
		char message[] = "HELLO, WORLD!";
		/** send the thing... */
		printf("Sending SMS to %s... ",phone);
		sprintf(buffer,"AT+CMGS=\"%s\"",phone);
		gsm_command(buffer);
		while(uart_read_byte(&port)!='>');
		pbuff = message;
		while(*pbuff)
		{
			uart_send_byte(&port,*pbuff);
			pbuff++;
		}
		uart_send_byte(&port,0x1a); // ctrl+z
		sleep(1); // got one whitespace char on tc35?
		count = gsm_replies(buffer,BUFF_SIZE,&okchk);
		if (count==0)
		{
			printf("no response?\n");
		}
		else if (okchk==GSM_OK_COMPLETE)
		{
			printf("OK.\n");
		}
		else
		{
			do_debug("invalid response.\n",buffer,count);
		}
	}
	putchar('\n');
	/* close port */
	uart_done(&port);
	/* free gpio */
	gpio_free();
	return 0;
}
/*----------------------------------------------------------------------------*/
