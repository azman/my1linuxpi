# common makefile to generate linux binary running on raspberry pi
PROJECT ?= main
SOURCES ?= $(PROJECT).c
OBJECTS ?= $(SOURCES:%.c=%.o)
TGTEXEC ?= $(PROJECT)

# compiler option
CFLAGS += -Wall
# linker option
LFLAGS +=

# specify module header location
CFLAGS += -I../zzz_modules/

# some require my1codelib project
EXTPATH = ../../my1codelib/src
CFLAGS += -I$(EXTPATH)/

THIS_PATH:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
ifneq ($(wildcard $(THIS_PATH)/zzz_modules),)

ALL_LIST=$(subst /makefile,,$(sort $(wildcard t*_*/makefile)))

info:
	@echo "Go to respective folders to build the tutorial code(s) you need!"

define deps_template
$(1): $(2)
endef

define make_template
$(1):
	@echo "-- Building $(1) ..."
	@make --no-print-directory -C $(2)
endef

define wipe_template
$(1):
	@echo -n "-- Cleaning $(1) => "
	@make --no-print-directory -C $(2) clean
endef

# create rule(s) for make_template
ALL_MAKE=$(foreach tuts, $(ALL_LIST), $(tuts)_make)
$(foreach tuts, $(ALL_LIST),$(eval $(call make_template,$(tuts)_make,$(tuts))))
# create rule(s) for wipe_template
ALL_WIPE=$(foreach tuts, $(ALL_LIST), $(tuts)_wipe)
$(foreach tuts, $(ALL_LIST),$(eval $(call wipe_template,$(tuts)_wipe,$(tuts))))
# shorter...
$(foreach tuts, $(ALL_LIST),$(eval $(call deps_template,$(shell echo $(tuts) | sed 's|t[^_]*_||'),$(tuts)_make)))
$(foreach tuts, $(ALL_LIST),$(eval $(call deps_template,$(shell echo $(tuts) | sed 's|t[^_]*_||')_wipe,$(tuts)_wipe)))

all: $(ALL_MAKE)

clean: $(ALL_WIPE)

else

all: $(TGTEXEC)

clean:
	rm -rf $(OBJECTS) $(TGTEXEC)

new: clean all

$(TGTEXEC): $(OBJECTS)
	gcc $(CFLAGS) $^ -o $@ $(LFLAGS)

%.o: %.c %.h
	gcc $(CFLAGS) -c $< -o $@

%.o: %.c
	gcc $(CFLAGS) -c $< -o $@

%.o: ../zzz_modules/%.c ../zzz_modules/%.h
	gcc $(CFLAGS) -c $< -o $@

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	gcc $(CFLAGS) -c $< -o $@

endif
