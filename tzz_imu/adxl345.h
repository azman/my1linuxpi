/*----------------------------------------------------------------------------*/
#ifndef __MY1ADXL345H__
#define __MY1ADXL345H__
/*----------------------------------------------------------------------------*/
#include "i2c.h"
/*----------------------------------------------------------------------------*/
/** 0x3a & 0x3b */
#define ADXL345_A1 0x1d
/** 0xa6 & 0xa7 */
#define ADXL345_A2 0x53
/*----------------------------------------------------------------------------*/
/* id register - on-reset value: 0xe5 */
#define DEV_ID 0x00
/* control registers */
#define PWRCTL 0x2d
#define DATFMT 0x31
/* power control register bit */
#define MEASURE 0x08
/* data format register bit */
#define FULLRES 0x08
#define DATJUST 0x04
#define RANGE02 0x00
#define RANGE04 0x01
#define RANGE08 0x02
#define RANGE16 0x03
/** data registers */
#define DATAX0 0x32
#define DATAX1 0x33
#define DATAY0 0x34
#define DATAY1 0x35
#define DATAZ0 0x36
#define DATAZ1 0x37
/*----------------------------------------------------------------------------*/
/* data scale */
#define DATA_SCALE 0.0078
/*----------------------------------------------------------------------------*/
typedef struct _adxl345_t
{
	int addr, flag;
	i2c_t *pi2c; /* initialize this externally */
	unsigned short data[3]; /* x,y,z data */
	float xacc, yacc, zacc;
}
adxl345_t;
/*----------------------------------------------------------------------------*/
void adxl345_init(adxl345_t* pacc, i2c_t* pick, int addr);
void adxl345_read(adxl345_t* pacc);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
