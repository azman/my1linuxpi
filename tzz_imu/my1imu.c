/*----------------------------------------------------------------------------*/
#include "i2c.h"
#include "gpio.h"
#include "adxl345.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define WAIT_DELAY 2000000
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	i2c_t that;
	adxl345_t accl;
	printf("Initialize I2C\n");
	fflush(stdout);
	gpio_init();
	i2c_init(&that,DEFAULT_SDA_GPIO,DEFAULT_SCL_GPIO);
	printf("Initialize ADXL345\n");
	fflush(stdout);
	adxl345_init(&accl,&that,ADXL345_A2);
	printf("-- Device:0x%02x\n",accl.flag);
	fflush(stdout);
	while (1)
	{
		adxl345_read(&accl);
		printf("-- X:%f , Y:%f , Z:%f\n",accl.xacc,accl.yacc,accl.zacc);
		fflush(stdout);
		timer_wait(1000000);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
