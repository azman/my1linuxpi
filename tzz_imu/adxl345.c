/*----------------------------------------------------------------------------*/
/**
 * - for accessing i2c
 *   = using bit-banging technique
**/
/*----------------------------------------------------------------------------*/
#include "adxl345.h"
#include "gpio.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
void adxl345_init(adxl345_t* pacc, i2c_t* pick, int addr)
{
	pacc->pi2c = pick; /* assumed initialized */
	pacc->addr = addr & 0x7F;
	/* set i2c clocking? */
	pacc->pi2c->i2c_wait = 10; /* 20us period -> 50kHz ? */
	pacc->pi2c->i2c_free = 10;
	/* get device id? */
	pacc->flag = i2c_getb(pacc->pi2c,pacc->addr,DEV_ID);
	/* setup data format - +/- 4g? full resolution? FULLRES| */
	i2c_putb(pacc->pi2c,pacc->addr,DATFMT,RANGE04);
	/* start measurement */
	i2c_putb(pacc->pi2c,pacc->addr,PWRCTL,MEASURE);
}
/*----------------------------------------------------------------------------*/
void adxl345_read(adxl345_t* pacc)
{
	/* read 6-bytes at once! */
	i2c_gets(pacc->pi2c,pacc->addr,DATAX0,(unsigned char*)pacc->data,6);
	pacc->xacc = pacc->data[0]*DATA_SCALE;
	pacc->yacc = pacc->data[1]*DATA_SCALE;
	pacc->zacc = pacc->data[2]*DATA_SCALE;
}
/*----------------------------------------------------------------------------*/
