#!/bin/bash

GPIO_SYSFS="/sys/class/gpio"
GPIO_TOOL=$(basename $0 .sh)

# new kernel interface: find gpiochip
CHIP_LIST=$(find $GPIO_SYSFS/ -name "gpiochip*")
GPIO_CHIP=
for CHIP in $CHIP_LIST ; do
	if [ -r $CHIP/ngpio ] ; then
		#echo "@@ CHIP:[$chip](${chip#*gpiochip})"
		GPIO_CHIP=$CHIP
		break;
	fi
done
[ -z "$GPIO_CHIP" ] &&
	echo "[$GPIO_TOOL] Cannot find gpiochip in sysfs!" && exit 1
GPIO_CCNT=$(cat $GPIO_CHIP/ngpio)
GPIO_COFF=${GPIO_CHIP#*gpiochip}

# if no param, show help and exit?
if [ -z "$1" ] ; then
	echo
	echo "$GPIO_TOOL - Raspberry Pi Basic GPIO Tool"
	echo "Usage:"
	echo "    $GPIO_TOOL [gpio_num] <command>"
	echo
	exit 0
elif [ "$1" = "--list" ] ; then
	LIST=$(ls $GPIO_SYSFS/gpio* | grep "$GPIO_SYSFS/gpio[0-9]" | sed 's/://g')
	if [ ! -z "$LIST" ]; then
		echo
		echo "$LIST"
		echo
	fi
	exit 0
fi

# first param is always gpio number
GPIO_NUM=$(echo $1 | sed 's/[^0-9]//g')
if [ -z "$GPIO_NUM" -o "$GPIO_NUM" != "$1" ] ; then
	echo
	echo "[$GPIO_TOOL] First parameter should be GPIO number"
	echo
	exit 1
fi

if [ $GPIO_NUM -lt 2 -o $GPIO_NUM -gt 27 ] ; then
	echo
	echo "[$GPIO_TOOL] Invalid GPIO number '$GPIO_NUM'! (Valid range: 2-27)"
	echo
	exit 1
fi
shift
((GPIO_CNUM=GPIO_NUM+GPIO_COFF))
GPIO_PATH=${GPIO_SYSFS}/gpio$GPIO_CNUM

function gpio_ready() {
	[ -d "$GPIO_PATH" ] && return 1
	return 0
}

function gpio_error() {
	echo "[$GPIO_TOOL] $@"
	exit 1
}

function gpio_export() {
	# check if already exported
	[ -d "$GPIO_PATH" ] && return
	# do your thing...
	echo -n "[$GPIO_TOOL] Exporting GPIO${GPIO_NUM}... "
	echo $GPIO_CNUM > $GPIO_SYSFS/export
	[ $? -ne 0 ] && echo -e "failed? ($?)\n" && exit 1
	echo "done."
	# is it really there?
	[ ! -d "$GPIO_PATH" ] && gpio_error "Something is wrong! Aborting!"
}

function gpio_unexport() {
	# check if not exported
	[ ! -d "$GPIO_PATH" ] && return
	# do your thing...
	echo -n "[$GPIO_TOOL] Releasing GPIO${GPIO_NUM}... "
	echo $GPIO_CNUM > $GPIO_SYSFS/unexport
	[ $? -ne 0 ] && echo -e "failed? ($?)\n" && exit 1
	echo "done."
	# is it really gone?
	[ -d "$GPIO_PATH" ] && gpio_error "Something is wrong! Aborting!"
}

# find command
GPIO_TASK=$1
if [ -z "$GPIO_TASK" ] ; then
	echo
	echo -n "[$GPIO_TOOL] GPIO$GPIO_NUM: "
	if [ -d "$GPIO_PATH" ] ; then
		echo "exported."
		echo "    Direction: "$(cat $GPIO_PATH/direction)
		echo "    Value: "$(cat $GPIO_PATH/value)
	else
		echo "not exported."
	fi
	echo
	exit 0
fi
echo
case $GPIO_TASK in
	input)
		gpio_export
		echo -n "[$GPIO_TOOL] Configure GPIO${GPIO_NUM} as input... "
		sleep 1
		echo "in" > ${GPIO_PATH}/direction
		echo "done."
		;;
	output)
		gpio_export
		echo -n "[$GPIO_TOOL] Configure GPIO${GPIO_NUM} as output... "
		sleep 1
		echo "out" > ${GPIO_PATH}/direction
		echo "done."
		;;
	release) gpio_unexport ;;
	high|hi|1)
		[ ! gpio_ready ] && gpio_error "GPIO not exported? Aborting!"
		[ $(cat $GPIO_PATH/direction) = "in" ] &&
			gpio_error "GPIO is an input pin? Aborting!"
		echo -n "[$GPIO_TOOL] Setting GPIO${GPIO_NUM} to high... "
		echo 1 > ${GPIO_PATH}/value
		echo "done."
		;;
	low|lo|0)
		[ ! gpio_ready ] && gpio_error "GPIO not exported? Aborting!"
		[ $(cat $GPIO_PATH/direction) = "in" ] &&
			gpio_error "GPIO is an input pin? Aborting!"
		echo -n "[$GPIO_TOOL] Setting GPIO${GPIO_NUM} to low... "
		echo 0 > ${GPIO_PATH}/value
		echo "done."
		;;
	read|value)
		[ ! gpio_ready ] && gpio_error "GPIO not exported? Aborting!"
		[ $(cat $GPIO_PATH/direction) != "in" ] &&
			gpio_error "GPIO is NOT an input pin? Aborting!"
		echo "[$GPIO_TOOL] GPIO${GPIO_NUM} value: "$(cat $GPIO_PATH/value)
		;;
	*) echo "[$GPIO_TOOL] Unknown command '$GPIO_TASK'!" ;;
esac
echo
