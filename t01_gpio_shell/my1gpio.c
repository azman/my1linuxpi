/*----------------------------------------------------------------------------*/
#include "my1gpio_sysfs.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, flag, gpio_num;
	char *pname = basename(argv[0]);
	/* if no param, show help and exit? */
	if (argc<2) {
		printf("\n%s - Raspberry Pi Basic GPIO Tool\n",pname);
		printf("Usage:\n");
		printf("    %s [gpio_num] <command>\n\n",pname);
		return 0;
	}
	/* if list is requested? */
	if (!strncmp(argv[1],"--list",6)) {
		flag = 1;
		for (loop=2;loop<28;loop++) {
			if (gpio_is_ready(loop)) {
				if (flag==1) {
					flag = 0;
					putchar('\n');
				}
				printf("/sys/class/gpio/gpio%d (%d)\n",gpio_chipnum(loop),loop);
			}
		}
		if (!flag) putchar('\n');
		return 0;
	}
	/* first param is always gpio number */
	gpio_num = atoi(argv[1]);
	if (gpio_num<2||gpio_num>27) {
		printf("\n[%s] Invalid GPIO number '%d'! (Valid range: 2-27)\n\n",
			pname,gpio_num);
		exit(1);
	}
	/* if no command, print stat and exit */
	if (argc<3) {
		printf("\n[%s] GPIO%d: ",pname,gpio_num);
		if (gpio_is_ready(gpio_num)) {
			printf("exported.\n");
			printf("    Direction: ");
			if (gpio_is_input(gpio_num)) printf("in\n");
			else printf("out\n");
			printf("    Value: ");
			if (gpio_read(gpio_num)) printf("1\n");
			else printf("0\n");
		}
		else printf("not exported.\n");
		putchar('\n');
		return 0;
	}
	/* check command */
	putchar('\n');
	if (!strncmp(argv[2],"input",5)) {
		if (!gpio_export(gpio_num)) {
			printf("Error exporting GPIO%d!\n\n",gpio_num);
			exit(1);
		}
		sleep(1);
		printf("[%s] Configure GPIO%d as input... ",pname,gpio_num);
		if (gpio_config(gpio_num,GPIO_INPUT)) printf("done.\n");
		else {
			printf("error?!\n\n");
			exit(1);
		}
	}
	else if (!strncmp(argv[2],"output",6)) {
		if (!gpio_export(gpio_num)) {
			printf("Error exporting GPIO%d!\n\n",gpio_num);
			exit(1);
		}
		sleep(1);
		printf("[%s] Configure GPIO%d as output... ",pname,gpio_num);
		if (gpio_config(gpio_num,GPIO_OUTPUT)) printf("done.\n");
		else {
			printf("error?!\n\n");
			exit(1);
		}
	}
	else if (!strncmp(argv[2],"release",7)) {
		if (!gpio_unexport(gpio_num)) {
			printf("Error unexporting GPIO%d!\n\n",gpio_num);
			exit(1);
		}
		printf("[%s] GPIO%d released.\n",pname,gpio_num);
	}
	else if (!strncmp(argv[2],"high",4)||!strncmp(argv[2],"hi",2)||
			!strncmp(argv[2],"1",1)) {
		if (!gpio_is_ready(gpio_num)) {
			printf("[%s] GPIO%d not exported? Aborting!\n\n",pname,gpio_num);
			exit(1);
		}
		if (gpio_is_input(gpio_num)) {
			printf("[%s] GPIO%d is an input pin! Aborting!\n\n",
				pname,gpio_num);
			exit(1);
		}
		printf("[%s] Setting GPIO%d to high... ",pname,gpio_num);
		if (gpio_set(gpio_num)) printf("done.\n");
		else printf("failed?!\n");
	}
	else if (!strncmp(argv[2],"low",3)||!strncmp(argv[2],"lo",2)||
			!strncmp(argv[2],"0",1)) {
		if (!gpio_is_ready(gpio_num)) {
			printf("[%s] GPIO%d not exported? Aborting!\n\n",pname,gpio_num);
			exit(1);
		}
		if (gpio_is_input(gpio_num)) {
			printf("[%s] GPIO%d is an input pin! Aborting!\n\n",
				pname,gpio_num);
			exit(1);
		}
		printf("[%s] Setting GPIO%d to low... ",pname,gpio_num);
		if (gpio_clr(gpio_num)) printf("done.\n");
		else printf("failed?!\n");
	}
	else if (!strncmp(argv[2],"read",4)||!strncmp(argv[2],"value",5)) {
		if (!gpio_is_ready(gpio_num)) {
			printf("[%s] GPIO%d not exported? Aborting!\n\n",pname,gpio_num);
			exit(1);
		}
		if (!gpio_is_input(gpio_num)) {
			printf("[%s] GPIO%d not an input pin! Aborting!\n\n",
				pname,gpio_num);
			exit(1);
		}
		printf("[%s] GPIO%d value: %d\n",pname,gpio_num,gpio_read(gpio_num));
	}
	else printf("[%s] Unknown command '%s'!\n",pname,argv[2]);
	putchar('\n');
	return 0;
}
/*----------------------------------------------------------------------------*/
