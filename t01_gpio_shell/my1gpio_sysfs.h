/*----------------------------------------------------------------------------*/
#ifndef __MY1GPIO_SYSFS_H__
#define __MY1GPIO_SYSFS_H__
/*----------------------------------------------------------------------------*/
#define GPIO_INPUT  0x00
#define GPIO_OUTPUT 0x01
/* these are not available using sysfs? */
#define GPIO_ALTF5  0x02
#define GPIO_ALTF4  0x03
#define GPIO_ALTF0  0x04
#define GPIO_ALTF1  0x05
#define GPIO_ALTF2  0x06
#define GPIO_ALTF3  0x07
/*----------------------------------------------------------------------------*/
/* functions as in my1barepi */
int gpio_config(int gpio_num, int select);
int gpio_set(int gpio_num);
int gpio_clr(int gpio_num);
int gpio_read(int gpio_num);
int gpio_toggle(int gpio_num);
/* additional functions */
int gpio_export(int gpio_num);
int gpio_unexport(int gpio_num);
int gpio_is_ready(int gpio_num);
int gpio_is_input(int gpio_num);
int gpio_write(int gpio_num, int value);
int gpio_free(int gpio_num);
int gpio_chipnum(int gpio_num);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
