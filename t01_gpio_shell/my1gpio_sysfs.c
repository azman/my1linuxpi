/*----------------------------------------------------------------------------*/
#include "my1gpio_sysfs.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
/*----------------------------------------------------------------------------*/
#define SYSFS_PATH "/sys/class/gpio"
/*----------------------------------------------------------------------------*/
#define SYSFS_NAME_SIZE 256
/*----------------------------------------------------------------------------*/
int gpio_chipnum(int gpio_num) {
	struct dirent *psub;
	DIR *path;
	FILE* pchk;
	char name[SYSFS_NAME_SIZE], *pbuf;
	int pnum,temp,pmax;
	/* prepare path name */
	sprintf(name,"%s",SYSFS_PATH);
	pnum = -1;
	path = opendir(name);
	if (path) {
		/* look for gpiochip */
		while ((psub=readdir(path))!=NULL) {
			if (!strncmp("gpiochip",psub->d_name,8)) {
				pbuf = strdup(psub->d_name);
				sprintf(name,"%s/%s/ngpio",SYSFS_PATH,pbuf);
				free((void*)pbuf);
				temp = atoi(&psub->d_name[8]);
				pchk = fopen(name,"r");
				if (pchk) {
					if (fscanf(pchk,"%d",&pmax)==1) {
						if (gpio_num<pmax)
							pnum = temp + gpio_num;
					}
					fclose(pchk);
				}
				break;
			}
		}
		closedir(path);
	}
	return pnum;
}
/*----------------------------------------------------------------------------*/
int gpio_is_ready(int gpio_num) {
	DIR* path;
	char name[SYSFS_NAME_SIZE];
	/* prepare path name */
	sprintf(name,"%s/gpio%d",SYSFS_PATH,gpio_chipnum(gpio_num));
	/* check if path exists (exported) */
	path = opendir(name);
	if (path) {
		closedir(path);
		return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_export(int gpio_num) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE];
	/* check if already exported */
	if (gpio_is_ready(gpio_num)) return 1;
	/* prepare path name */
	sprintf(name,"%s/export",SYSFS_PATH);
	/* try to export */
	pfile = fopen(name,"w");
	if (pfile) {
		fprintf(pfile,"%d",gpio_chipnum(gpio_num));
		fclose(pfile);
		if (gpio_is_ready(gpio_num)) return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_unexport(int gpio_num) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE];
	/* check if already exported */
	if (!gpio_is_ready(gpio_num)) return 1;
	/* prepare path name */
	sprintf(name,"%s/unexport",SYSFS_PATH);
	/* try to export */
	pfile = fopen(name,"w");
	if (pfile) {
		fprintf(pfile,"%d",gpio_chipnum(gpio_num));
		fclose(pfile);
		if (!gpio_is_ready(gpio_num)) return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
#define DIR_BUFF_SIZE 8
/*----------------------------------------------------------------------------*/
int gpio_is_input(int gpio_num) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE], buff[DIR_BUFF_SIZE];
	/* prepare path name */
	sprintf(name,"%s/gpio%d/direction",SYSFS_PATH,gpio_chipnum(gpio_num));
	/* check direction */
	pfile = fopen(name,"r");
	if (pfile) {
		fgets(buff,DIR_BUFF_SIZE,pfile);
		fclose(pfile);
		if (buff[0]=='i'&&buff[1]=='n') return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_config(int gpio_num, int select) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE];
	/* need to export */
	if (!gpio_export(gpio_num)) return 0;
	/* prepare path name */
	sprintf(name,"%s/gpio%d/direction",SYSFS_PATH,gpio_chipnum(gpio_num));
	/* configure direction */
	pfile = fopen(name,"w");
	if (pfile) {
		switch (select) {
			case GPIO_OUTPUT: fprintf(pfile,"out"); break;
			default:
			case GPIO_INPUT: fprintf(pfile,"in"); break;
		}
		fclose(pfile);
		/* now, we check */
		switch (select) {
			case GPIO_OUTPUT: if (!gpio_is_input(gpio_num)) return 1; break;
			case GPIO_INPUT: if (gpio_is_input(gpio_num)) return 1; break;
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_set(int gpio_num) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE];
	/* prepare path name */
	sprintf(name,"%s/gpio%d/value",SYSFS_PATH,gpio_chipnum(gpio_num));
	/* try to set */
	pfile = fopen(name,"w");
	if (pfile) {
		fprintf(pfile,"1");
		fclose(pfile);
		return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_clr(int gpio_num) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE];
	/* prepare path name */
	sprintf(name,"%s/gpio%d/value",SYSFS_PATH,gpio_chipnum(gpio_num));
	/* try to set */
	pfile = fopen(name,"w");
	if (pfile) {
		fprintf(pfile,"0");
		fclose(pfile);
		return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_read(int gpio_num) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE], buff[DIR_BUFF_SIZE];
	/* prepare path name */
	sprintf(name,"%s/gpio%d/value",SYSFS_PATH,gpio_chipnum(gpio_num));
	/* try to set */
	pfile = fopen(name,"r");
	if (pfile) {
		fgets(buff,DIR_BUFF_SIZE,pfile);
		fclose(pfile);
		if (buff[0]=='1') return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_toggle(int gpio_num) {
	if(gpio_read(gpio_num)) gpio_clr(gpio_num);
	else gpio_set(gpio_num);
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_write(int gpio_num, int value) {
	FILE* pfile;
	char name[SYSFS_NAME_SIZE];
	/* prepare path name */
	sprintf(name,"%s/gpio%d/value",SYSFS_PATH,gpio_chipnum(gpio_num));
	/* try to set */
	pfile = fopen(name,"w");
	if (pfile) {
		fprintf(pfile,"%d",value?1:0);
		fclose(pfile);
		return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int gpio_free(int gpio_num) {
	return gpio_unexport(gpio_num);
}
/*----------------------------------------------------------------------------*/
