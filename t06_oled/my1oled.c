/*----------------------------------------------------------------------------*/
#include "i2c.h"
#include "gpio.h"
#include "oled1306.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define WAIT_DELAY 3000000
/*----------------------------------------------------------------------------*/
#define SDA_GPIO 20
#define SCL_GPIO 21
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	i2c_t that;
	oled1306_t show;
	printf("Initialize I2C\n");
	fflush(stdout);
	gpio_init();
	i2c_init(&that,SDA_GPIO,SCL_GPIO);
	that.i2c_wait = 15;
	printf("Initialize OLED\n");
	fflush(stdout);
	oled1306_init(&show,&that,SSD1306_ADDRESS,OLED_TYPE_128x64,0x0);
	printf("LIGHT OLED\n");
	fflush(stdout);
	oled1306_fill(&show);
	oled1306_update(&show);
	timer_wait(WAIT_DELAY);
	printf("CLEAR OLED\n");
	fflush(stdout);
	oled1306_clear(&show);
	oled1306_update(&show);
	timer_wait(WAIT_DELAY);
	printf("SHOW SOMETHING\n");
	fflush(stdout);
	oled1306_cursor(&show,0,0);
	oled1306_text(&show,"YAN BAJANG!");
	oled1306_cursor(&show,2,0);
	oled1306_text(&show,"MY1 KING!");
	oled1306_update(&show);
	timer_wait(WAIT_DELAY);
	printf("CLEAR OLED\n");
	fflush(stdout);
	oled1306_clear(&show);
	oled1306_update(&show);
	timer_wait(WAIT_DELAY);
	printf("Turning OFF OLED\n");
	oled1306_switch(&show,0);
	return 0;
}
/*----------------------------------------------------------------------------*/
