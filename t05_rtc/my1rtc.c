/*----------------------------------------------------------------------------*/
#include "i2c.h"
#include "gpio.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define WAIT_DELAY 200000
/* ds1307 */
#define RTC_I2C_ADDR 0x68
/*----------------------------------------------------------------------------*/
#define SDA_GPIO DEFAULT_SDA_GPIO
#define SCL_GPIO DEFAULT_SCL_GPIO
#define CURRCENT 20
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void ds1307_halt(i2c_t* pdev, int halt) {
	int temp;
	temp = i2c_getb(pdev,RTC_I2C_ADDR,0x00);
	if (halt) temp |= 0x80;
	else temp &= 0x7f;
	i2c_putb(pdev,RTC_I2C_ADDR,0x00,temp);
}
/*----------------------------------------------------------------------------*/
void ds1307_year(i2c_t* pdev, int year) {
	unsigned char temp;
	temp = (unsigned char)year%10;
	year /= 10;
	if (year>9) year = 0;
	temp |= (unsigned char)year<<4;
	i2c_putb(pdev,RTC_I2C_ADDR,0x06,temp);
}
/*----------------------------------------------------------------------------*/
void ds1307_month(i2c_t* pdev, int month) {
	unsigned char temp;
	if (month<1||month>12) return;
	if (month>9) {
		temp = 0x10;
		month -= 10;
	}
	else temp = 0;
	temp += month;
	i2c_putb(pdev,RTC_I2C_ADDR,0x05,temp);
}
/*----------------------------------------------------------------------------*/
void ds1307_date(i2c_t* pdev, int date) {
	unsigned char temp;
	temp = (unsigned char)date%10;
	date /= 10;
	if (date>3) date = 0;
	temp |= (unsigned char)date<<4;
	i2c_putb(pdev,RTC_I2C_ADDR,0x04,temp);
}
/*----------------------------------------------------------------------------*/
void ds1307_hour(i2c_t* pdev, int hour) {
	unsigned char temp;
	temp = (unsigned char)hour%10;
	hour /= 10;
	if (hour>2) hour = 0;
	temp |= (unsigned char)hour<<4;
	i2c_putb(pdev,RTC_I2C_ADDR,0x02,temp);
}
/*----------------------------------------------------------------------------*/
void ds1307_mmin(i2c_t* pdev, int mmin) {
	unsigned char temp;
	temp = (unsigned char)mmin%10;
	mmin /= 10;
	if (mmin>5) mmin = 0;
	temp |= (unsigned char)mmin<<4;
	i2c_putb(pdev,RTC_I2C_ADDR,0x01,temp);
}
/*----------------------------------------------------------------------------*/
void ds1307_ssec(i2c_t* pdev, int ssec) {
	unsigned char temp;
	temp = (unsigned char)ssec%10;
	ssec /= 10;
	if (ssec>5) ssec = 0;
	temp |= (unsigned char)ssec<<4;
	i2c_putb(pdev,RTC_I2C_ADDR,0x00,temp|0x80);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	i2c_t that;
	unsigned char data[8];
	int temp, tick;
	printf("-- Initialize I2C\n");
	fflush(stdout);
	gpio_init();
	i2c_init(&that,SDA_GPIO,SCL_GPIO);
	/* options */
	for (temp=1;temp<argc;temp++) {
		if (!strncmp(argv[temp],"--year",7)) {
			if (++temp<argc) {
				tick = atoi(argv[temp]);
				if (tick>=0&&tick<100) {
					ds1307_halt(&that,1);
					ds1307_year(&that,tick);
				}
			}
		}
		else if (!strncmp(argv[temp],"--month",8)) {
			if (++temp<argc) {
				tick = atoi(argv[temp]);
				if (tick>0&&tick<=12) {
					ds1307_halt(&that,1);
					ds1307_month(&that,tick);
				}
			}
		}
		else if (!strncmp(argv[temp],"--date",7)) {
			if (++temp<argc) {
				tick = atoi(argv[temp]);
				if (tick>0&&tick<=31) {
					ds1307_halt(&that,1);
					ds1307_date(&that,tick);
				}
			}
		}
		else if (!strncmp(argv[temp],"--hour",7)) {
			if (++temp<argc) {
				tick = atoi(argv[temp]);
				if (tick>=0&&tick<=23) {
					ds1307_halt(&that,1);
					ds1307_hour(&that,tick);
				}
			}
		}
		else if (!strncmp(argv[temp],"--min",6)) {
			if (++temp<argc) {
				tick = atoi(argv[temp]);
				if (tick>=0&&tick<=59) {
					ds1307_halt(&that,1);
					ds1307_mmin(&that,tick);
				}
			}
		}
		else if (!strncmp(argv[temp],"--sec",6)) {
			if (++temp<argc) {
				tick = atoi(argv[temp]);
				if (tick>=0&&tick<=59) {
					ds1307_halt(&that,1);
					ds1307_ssec(&that,tick);
				}
			}
		}
	}
	/* prepare stuffs */
	ds1307_halt(&that,0);
	tick = -1;
	printf("-- Running...\n");
	fflush(stdout);
	while (1) {
		temp = i2c_gets(&that,RTC_I2C_ADDR,0x00,data,8);
		if (temp) continue;
		/* get time */
		data[0] = (((data[0]&0x70)>>4)*10)+(data[0]&0xf); /* second */
		data[1] = (((data[1]&0x70)>>4)*10)+(data[1]&0xf); /* minute */
		if (data[2]&0x40) /* 12-h */ {
			temp = data[2]&0x20;
			data[2] = (((data[2]&0x10)>>4)*10)+(data[2]&0xf);
			if (temp) data[2] += 12;
		}
		else /* 24-h */
			data[2] = (((data[2]&0x30)>>4)*10)+(data[2]&0xf);
		/* get date */
		data[3] = data[3]&0x07; /* day 1-7 */
		data[4] = (((data[4]&0x30)>>4)*10)+(data[4]&0xf); /* date */
		data[5] = (((data[5]&0x10)>>4)*10)+(data[5]&0xf); /* month */
		data[6] = (((data[6]&0xf0)>>4)*10)+(data[6]&0xf); /* year 0-99 */
		/* display */
		if (tick<0||tick!=data[0]) {
			tick = data[0];
			printf("%02d%02d-%02d-%02d ",CURRCENT,data[6],data[5],data[4]);
			printf("%02d:%02d:%02d\n",data[2],data[1],data[0]);
		}
		timer_wait(WAIT_DELAY);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
