/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "timer.h"
#include "frc522.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
int main(void) {
	spi_t spid;
	int loop, test, size;
	unsigned int last;
	unsigned char pdat[FRC522_MAX_RXSIZE];
	/** initialize spi */
	spi_init(&spid,0);
	test = frc522_reg_read(&spid,FRC522_P3_VERSION_REG);
	if (test!=0x92&&test!=0x91) {
		printf("\n@@ Invalid Firmware? (%x)\n",test);
		return -1;
	}
	/* initialize mf contactless card reader */
	test = frc522_init(&spid);
	/** say something... */
	printf("--------------------\n");
	printf("MF Card Reader Test!\n");
	printf("--------------------\n\n");
	if (!test||(test&0xff)==0xff)
		printf("** Cannot find FRC522 hardware! (%x)\n",test);
	else printf("-- FRC522 found. Firmware version is 0x%x.\n",test);
	last = timer_read();
	/** main loop */
	while (1) {
		if (get_keyhit()==KEY_ESCAPE) {
			fflush(stdin); fflush(stdout);
			break;
		}
		test = frc522_scan(&spid,pdat,&size);
		if (test==FRC522_OK) {
			if (timer_read()-last<3000000) continue;
			printf("## TAG(%02x,%02x):",pdat[FRC522_MAX_RXSIZE-2],
				pdat[FRC522_MAX_RXSIZE-1]);
			for (loop=0;loop<size;loop++)
				printf("[%02x]",pdat[loop]);
			printf("\n");
			last = timer_read();
		}
		else if (test!=FRC522_ERROR_NO_TAG&&test!=FRC522_ERROR_REQ_A) {
			printf("** Scan Failed (0x%x):",test);
			for (loop=0;loop<FRC522_MAX_RXSIZE;loop++)
				printf("[%02x]",pdat[loop]);
			printf("{last:%u}{tlen:%u}",spid.last,spid.curr);
			printf("\n");
		}
	}
	printf("\n@\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
