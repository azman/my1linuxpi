/*----------------------------------------------------------------------------*/
#include "gpio.h"
#include "my1sock.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* fcntl */
#include <fcntl.h>
/* getcwd */
#include <unistd.h>
/*----------------------------------------------------------------------------*/
char* append_char_array(char **pbuffer, int *pcount, char *pdata)
{
	int count,total;
	char *ptest;
	count = strlen(pdata);
	if((*pcount)==0) count++;
	total = (*pcount) + count;
	ptest = (char*) realloc((void*)*pbuffer, total);
	if(ptest)
	{
		if((*pcount)==0) ptest[0] = 0x0;
		strcat(ptest,pdata);
		*pbuffer = ptest;
		*pcount = total;
	}
	return ptest;
}
/*----------------------------------------------------------------------------*/
/* the rest */
#include <errno.h>
/*----------------------------------------------------------------------------*/
#define HTTP_BUFFSIZE 4096
#define TEMP_BUFFSIZE 32
#define FILE_NAMESIZE 128
#define OK_TEXT "HTTP/1.0 200 OK\r\nContent-Type:text/html\r\n\r\n"
#define OK_DONE "<html><body><h1>Success</h1></body></html>"
#define KO_404 "HTTP/1.0 404 Not Found\r\nContent-Type:text/html\r\n\r\n"
#define TO_404 "<html><body><h1>FILE NOT FOUND</h1></body></html>"
#define IO_404 "<html><body><h1>Invalid Request</h1></body></html>"
/*----------------------------------------------------------------------------*/
char* get_next_token(char** token,int* size,char* path,int* next)
{
	char* found = 0x0;
	int loop = *next;
	*size = 0;
	while (path[loop])
	{
		if (path[loop]=='/')
		{
			*token = &path[*next];
			*next = loop+1;
			found = *token;
			break;
		}
		loop++;
		(*size)++;
	}
	if (!found&&(*size)>0)
	{
		*token = &path[*next];
		*next = -1;
		found = *token;
	}
	return found;
}
/*----------------------------------------------------------------------------*/
#define SERVER_XOPTS_VERBOSE 0x01
/*----------------------------------------------------------------------------*/
int server_code(void* stuffs)
{
	my1conn_t *pcon = (my1conn_t*) stuffs;
	char ibuf[HTTP_BUFFSIZE];
	char obuf[HTTP_BUFFSIZE];
	int psize = 0, flag = 0, temp;
	char command[TEMP_BUFFSIZE];
	char filename[FILE_NAMESIZE];
	char *pbuff = 0x0;
	/* initialize gpio */
	gpio_init();
	/* notify? */
	printf("Request from {%s}!\n",pcon->ipv4addr);
	/* loop to get all request data */
	while(1)
	{
		temp = socket_getdata(pcon->sock,HTTP_BUFFSIZE-1,(void*)ibuf);
		if (temp>0)
		{
			append_char_array(&pbuff,&psize,ibuf); /* assume okay */
			if (temp<HTTP_BUFFSIZE) break;
		}
		else if (temp<0)
		{
			/* continue only if errno is EAGAIN or EWOULDBLOCK? */
			if (errno!=EAGAIN&&errno!=EWOULDBLOCK)
			{
				flag = -1; /* continue listening */
				break;
			}
		}
		else break;
	}
	/* check input buffer */
	if (!pbuff)
	{
		printf("Cannot get data from %s?!\n",pcon->ipv4addr);
		return 0;
	}
	if (flag<0)
	{
		printf("Cannot get socket data! (%d)\n",errno);
		flag = 0;
	}
	else
	{
		/* check HTTP's GET plus FILE... and confirm protocol? */
		sscanf(pbuff,"%s %s \n",command,filename);
		if (strcmp(command,"GET")||filename[0]!='/')
		{
			printf("Unknown request:\n%s\n",pbuff);
		}
		else
		{
			char *ptoken, buffer[80];
			int pcount, gpio_num = 0, temp = 1;
			if (pcon->psrvr->xopts&SERVER_XOPTS_VERBOSE)
				printf("Full request:[BEGIN]\n%s\nFull request:[END]\n",pbuff);
			printf("Looking for API request... (%s)\n",filename);
			/* assume api server... look for command */
			if (get_next_token(&ptoken,&pcount,filename,&temp)&&pcount==4)
			{
				if (!strncmp(ptoken,"gpio",4)&&temp>0)
				{
					if (get_next_token(&ptoken,&pcount,filename,&temp))
					{
						snprintf(buffer,80,"%s",ptoken);
						gpio_num = atoi(buffer);
						if (gpio_num<2||gpio_num>27) gpio_num = 0;
					}
				}
			}
			if (gpio_num&&temp>0)
			{
				int check = -1;
				if (get_next_token(&ptoken,&pcount,filename,&temp))
				{
					switch(pcount)
					{
						case 1:
							if (ptoken[0]=='0') check = 0;
							else if (ptoken[0]=='1') check = 1;
							break;
						case 2:
							if (!strncmp(ptoken,"lo",pcount)) check = 0;
							else if (!strncmp(ptoken,"hi",pcount)) check = 1;
							break;
						case 3:
							if (!strncmp(ptoken,"low",pcount)) check = 0;
							break;
						case 4:
							if (!strncmp(ptoken,"high",pcount)) check = 1;
							break;
					}
				}
				if (check<0) gpio_num = 0;
				else
				{
					if (!check)
					{
						gpio_config(gpio_num,GPIO_OUTPUT);
						gpio_clr(gpio_num);
						printf("## GPIO %d set to LO!\n",gpio_num);
					}
					else
					{
						gpio_config(gpio_num,GPIO_OUTPUT);
						gpio_set(gpio_num);
						printf("## GPIO %d set to HI!\n",gpio_num);
					}
				}
			}
			/* set browser message */
			if (gpio_num)
			{
				strcpy(obuf,OK_TEXT);
				strcat(obuf,OK_DONE);
			}
			else
			{
				strcpy(obuf,KO_404);
				strcat(obuf,IO_404);
			}
			socket_putdata(pcon->sock,strlen(obuf),(void*)obuf);
		}
	}
	/* cleanup */
	if (pbuff) free((void*)pbuff);
	/* free gpio */
	gpio_free();
	/* return non-zero to exit server main loop */
	return flag;
}
/*----------------------------------------------------------------------------*/
#include <termios.h> /* struct termios */
/*----------------------------------------------------------------------------*/
#define MASK_LFLAG (ICANON|ECHO|ECHOE|ISIG)
/*----------------------------------------------------------------------------*/
int getch(void)
{
	struct termios oldt, newt;
	int ch;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~MASK_LFLAG;
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	return ch;
}
/*----------------------------------------------------------------------------*/
int kbhit(void)
{
	struct termios oldt, newt;
	int ch;
	int oldf;

	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~MASK_LFLAG;
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	fcntl(STDIN_FILENO, F_SETFL, oldf);

	if(ch != EOF)
	{
		ungetc(ch, stdin);
		return 1;
	}

	return 0;
}
/*----------------------------------------------------------------------------*/
int server_done(void* stuffs)
{
	my1server_t* psrv = (my1server_t*)stuffs;
	if (kbhit())
	{
		switch (getch())
		{
			case (int)0x1b: /* escape key */
				printf("\nUser Abort Request!\n");
				return 1;
			case (int)0x20: /* space bar */
				psrv->xopts ^= SERVER_XOPTS_VERBOSE;
				if (psrv->xopts&SERVER_XOPTS_VERBOSE)
					printf("\n## Verbose Mode ON!\n");
				else
					printf("\n## Verbose Mode OFF!\n");
				break;
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	int loop, hostport=8080;
	my1sock_t sock;
	/* process parameters */
	for (loop=1;loop<argc;loop++)
	{
		if (argv[loop][0]=='-'&&argv[loop][1]=='-')
		{
			if (!strcmp(&argv[loop][2],"port"))
			{
				loop++;
				hostport = atoi(argv[loop]);
			}
			else
			{
				printf("Unknown option '%s'! Aborting!\n",argv[loop]);
				exit(1);
			}
		}
		else
		{
			printf("Unknown parameter '%s'! Aborting!\n",argv[loop]);
			exit(1);
		}
	}
	/** init default */
	socket_init(&sock);
	/** dummy loop */
	do
	{
		/* try to connect to socket */
		socket_create(&sock);
		if (sock.sock==INVALID_SOCKET)
		{
			printf("Cannot create socket! Aborting! (%d)\n",sock.flag);
			break;
		}
		{
			char* path;
			my1server_t server;
			server_init(&server,hostport);
			server.pcode = &server_code;
			server.pdone = &server_done;
			/* check current working dir - malloc'ed pathname */
			path = getcwd(0x0,0);
			server.pdata = (void*) path;
			/* set self host info */
			sock.chkadd.sin_family = AF_INET;
			sock.chkadd.sin_port = htons(hostport);
			sock.chkadd.sin_addr.s_addr = htonl(INADDR_ANY);
			/* start hosting clients */
			printf("Running server @port %d (%s)!\n",hostport,path);
			printf("Press <ESC> to exit.\n");
			socket_active(&sock,&server);
			/* free up resources */
			server_free(&server);
			/* finishing up... */
			printf("Server code done with code %d!\n",sock.flag);
			socket_finish(&sock);
		}
	}
	while(0); /* end of dummy loop */
	/* release socket */
	socket_free(&sock);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
