/*----------------------------------------------------------------------------*/
#ifndef __MY1I2C_H__
#define __MY1I2C_H__
/*----------------------------------------------------------------------------*/
/**
 * - bit-bang i2c lib
 *   = scl high/low time:(standard) >4.0us/>4.7us
 *   = scl high/low time:(fast) >1.3us/>0.6us
 *   = scl high/low time:(fast+) >0.5us/>0.26us
**/
/*----------------------------------------------------------------------------*/
typedef struct _i2c_t {
	int gpio_sda, gpio_scl; /* gpio_num used for sda/scl */
	unsigned int i2c_wait, i2c_temp;
} i2c_t;
/*----------------------------------------------------------------------------*/
/* NOT using hardware i2c pin */
#define DEFAULT_SDA_GPIO 20
#define DEFAULT_SCL_GPIO 21
/*----------------------------------------------------------------------------*/
#define I2C_WAIT_DEFAULT 5
/*----------------------------------------------------------------------------*/
/* 'low' level i2c signals - customized transfer... */
void i2c_do_start(i2c_t* pi2c);
void i2c_do_stop(i2c_t* pi2c);
int i2c_do_send(i2c_t* pi2c, int data);
int i2c_do_read(i2c_t* pi2c, int last);
/*----------------------------------------------------------------------------*/
void i2c_init(i2c_t* pi2c, int sda_gpio, int scl_gpio);
int i2c_putb(i2c_t* pi2c, int addr, int regs, int data);
int i2c_getb(i2c_t* pi2c, int addr, int regs);
int i2c_puts(i2c_t* pi2c, int addr, int regs, unsigned char* pdat, int size);
int i2c_gets(i2c_t* pi2c, int addr, int regs, unsigned char* pdat, int size);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
