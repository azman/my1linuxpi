/*----------------------------------------------------------------------------*/
#include "timer.h"
/*----------------------------------------------------------------------------*/
#include <sys/time.h>
/*----------------------------------------------------------------------------*/
int timer_timeout(unsigned int delay_us, timeout_check checker) {
	struct timeval init, curr, stop;
	gettimeofday(&init,0x0);
	curr.tv_sec = delay_us/1000000;
	curr.tv_usec = delay_us%1000000;
	timeradd(&init,&curr,&stop);
	do {
		gettimeofday(&curr,0x0);
		if (!(*checker)()) return 0;
	} while (timercmp(&curr,&stop,<));
	return 1;
}
/*----------------------------------------------------------------------------*/
void timer_wait(unsigned int delay_us) {
	struct timeval init, curr, stop;
	gettimeofday(&init,0x0);
	curr.tv_sec = delay_us/1000000;
	curr.tv_usec = delay_us%1000000;
	timeradd(&init,&curr,&stop);
	do {
		gettimeofday(&curr,0x0);
	} while (timercmp(&curr,&stop,<));
}
/*----------------------------------------------------------------------------*/
unsigned int timer_read(void) {
	struct timeval curr;
	unsigned int tsum;
	gettimeofday(&curr,0x0);
	tsum = (unsigned int)curr.tv_sec;
	tsum *= 1000000;
	tsum += (unsigned int)curr.tv_usec;
	return tsum;
}
/*----------------------------------------------------------------------------*/
