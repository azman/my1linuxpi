/*----------------------------------------------------------------------------*/
#include "i2c.h"
#include "gpio.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
/** i2c start marker */
void i2c_do_start(i2c_t* pi2c) {
	gpio_config(pi2c->gpio_scl,GPIO_OUTPUT);
	gpio_config(pi2c->gpio_sda,GPIO_OUTPUT);
	gpio_set(pi2c->gpio_scl);
	gpio_set(pi2c->gpio_sda);
	timer_wait(pi2c->i2c_wait); /* start condition setup time >4.7us */
	gpio_clr(pi2c->gpio_sda);
	timer_wait(pi2c->i2c_wait); /* start condition hold time > 4.0us */
	gpio_clr(pi2c->gpio_scl);
}
/*----------------------------------------------------------------------------*/
/** i2c stop marker */
void i2c_do_stop(i2c_t* pi2c) {
	gpio_clr(pi2c->gpio_sda);
	gpio_set(pi2c->gpio_scl);
	/* clock stretching -> slave pulls scl low! */
	timer_wait(pi2c->i2c_wait); /* stop condition setup time > 4.0us */
	gpio_set(pi2c->gpio_sda);
	timer_wait(pi2c->i2c_wait); /* bus free time before STA >4.7u */
	gpio_config(pi2c->gpio_sda,GPIO_INPUT);
	gpio_config(pi2c->gpio_scl,GPIO_INPUT);
}
/*----------------------------------------------------------------------------*/
/** i2c send byte */
int i2c_do_send(i2c_t* pi2c, int data) {
	int mask, stat;
	mask = 0x80;
	while (mask) {
		/* data transitions on scl low! */
		if (data&mask) gpio_set(pi2c->gpio_sda);
		else gpio_clr(pi2c->gpio_sda);
		timer_wait(1); /* data setup time: 0.25us? */
		gpio_set(pi2c->gpio_scl);
		timer_wait(pi2c->i2c_wait); /* scl high/low time: >4.0us/>4.7us */
		gpio_clr(pi2c->gpio_scl);
		timer_wait(pi2c->i2c_wait>>1);
		mask >>= 1;
	}
	gpio_set(pi2c->gpio_sda);
	gpio_config(pi2c->gpio_sda,GPIO_INPUT);
	gpio_set(pi2c->gpio_scl);
	timer_wait(pi2c->i2c_wait>>1);
	/* check acknowledge bit */
	stat = gpio_read(pi2c->gpio_sda) ? 1 : 0;
	timer_wait(pi2c->i2c_wait>>1);
	gpio_clr(pi2c->gpio_scl);
	gpio_config(pi2c->gpio_sda,GPIO_OUTPUT);
	return stat; /* returns zero if acknowledged */
}
/*----------------------------------------------------------------------------*/
/** i2c read byte */
int i2c_do_read(i2c_t* pi2c, int last) {
	int loop, data;
	gpio_set(pi2c->gpio_sda);
	gpio_config(pi2c->gpio_sda,GPIO_INPUT);
	for (loop=0,data=0x0;loop<8;loop++) {
		gpio_clr(pi2c->gpio_scl);
		timer_wait(pi2c->i2c_wait); /* scl high/low time: >4.0us/>4.7us */
		gpio_set(pi2c->gpio_scl);
		timer_wait(pi2c->i2c_wait>>1);
		data <<= 1;
		if (gpio_read(pi2c->gpio_sda)) data |= 0x01;
		//timer_wait(pi2c->i2c_wait>>1);
	}
	gpio_clr(pi2c->gpio_scl);
	//timer_wait(1); /* scl low to data valid time < 3.4us?? */
	gpio_config(pi2c->gpio_sda,GPIO_OUTPUT);
	if (!last) gpio_clr(pi2c->gpio_sda); /* send ack bit */
	gpio_set(pi2c->gpio_scl);
	timer_wait(pi2c->i2c_wait>>1);
	gpio_clr(pi2c->gpio_scl);
	return data;
}
/*----------------------------------------------------------------------------*/
void i2c_init(i2c_t* pi2c, int sda_gpio, int scl_gpio) {
	/* assign gpio */
	pi2c->gpio_sda = sda_gpio;
	pi2c->gpio_scl = scl_gpio;
	gpio_init();
	/* set pull-up on pins */
	gpio_pull(pi2c->gpio_sda,GPIO_PULL_UP);
	gpio_pull(pi2c->gpio_scl,GPIO_PULL_UP);
	/* just prepare this */
	gpio_config(pi2c->gpio_sda,GPIO_OUTPUT);
	gpio_config(pi2c->gpio_scl,GPIO_OUTPUT);
	gpio_set(pi2c->gpio_sda);
	gpio_set(pi2c->gpio_scl);
	/* leave tri-stated by default (input!) */
	gpio_config(pi2c->gpio_sda,GPIO_INPUT);
	gpio_config(pi2c->gpio_scl,GPIO_INPUT);
	/* default parameters */
	pi2c->i2c_wait = I2C_WAIT_DEFAULT;
}
/*----------------------------------------------------------------------------*/
int i2c_putb(i2c_t* pi2c, int addr, int regs, int data) {
	int test;
	test = 0;
	addr <<= 1;
	i2c_do_start(pi2c);
	test |= i2c_do_send(pi2c,addr);
	test |= i2c_do_send(pi2c,regs);
	test |= i2c_do_send(pi2c,data);
	i2c_do_stop(pi2c);
	return test;
}
/*----------------------------------------------------------------------------*/
int i2c_getb(i2c_t* pi2c, int addr, int regs) {
	int data;
	addr <<= 1;
	i2c_do_start(pi2c);
	i2c_do_send(pi2c,addr); /* ignore ack? */
	i2c_do_send(pi2c,regs);
	i2c_do_start(pi2c); /* send a restart for read */
	i2c_do_send(pi2c,addr|0x01); /* activate read bit */
	data = i2c_do_read(pi2c,1);
	i2c_do_stop(pi2c);
	return data & 0xff;
}
/*----------------------------------------------------------------------------*/
int i2c_puts(i2c_t* pi2c, int addr, int regs, unsigned char* pdat, int size) {
	int loop, test;
	test = 0; addr <<= 1;
	i2c_do_start(pi2c);
	test |= i2c_do_send(pi2c,addr);
	test |= i2c_do_send(pi2c,regs);
	for (loop=0;loop<size;loop++)
		test |= i2c_do_send(pi2c,pdat[loop]);
	i2c_do_stop(pi2c);
	return test;
}
/*----------------------------------------------------------------------------*/
int i2c_gets(i2c_t* pi2c, int addr, int regs, unsigned char* pdat, int size) {
	int loop, test;
	test = 0; addr <<= 1;
	i2c_do_start(pi2c);
	test |= i2c_do_send(pi2c,addr); test<<=1;
	test |= i2c_do_send(pi2c,regs); test<<=1;
	i2c_do_start(pi2c); /* send a restart for read */
	test |= i2c_do_send(pi2c,addr|0x01); /* activate read bit */ test<<=1;
	for (loop=0;loop<size-1;loop++)
		pdat[loop] = i2c_do_read(pi2c,0);
	pdat[loop] = i2c_do_read(pi2c,1);
	i2c_do_stop(pi2c);
	return test;
}
/*----------------------------------------------------------------------------*/
