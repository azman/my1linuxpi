/*----------------------------------------------------------------------------*/
#include "gpio.h"
/*----------------------------------------------------------------------------*/
#include <fcntl.h> /* open? */
#include <unistd.h> /* close? */
#include <sys/mman.h> /* mmap, munmap */
/*----------------------------------------------------------------------------*/
#define GPIO_OFFSET 0x00200000
/*----------------------------------------------------------------------------*/
#define GPIO_FSEL 0x00
#define GPIO_FSET 0x07
#define GPIO_FCLR 0x0A
#define GPIO_FGET 0x0D
#define GPIO_EVDS 0x10
#define GPIO_EREN 0x13
#define GPIO_EFEN 0x16
#define GPIO_LHEN 0x19
#define GPIO_LLEN 0x1C
#define GPIO_AREN 0x1F
#define GPIO_AFEN 0x22
#define GPIO_FPUD 0x25
/*----------------------------------------------------------------------------*/
#define GPIO_SELECT_BITS 3
#define GPIO_SELECT 0x07
/*----------------------------------------------------------------------------*/
#define GPIO_DATA_OFFSET 20
#define GPIO_DATA_DECADE 2
/** 0010_0100_1001_0010_0100_1001 */
#define GPIO_DATA_OUTPUT 0x00249249
#define GPIO_DATA_DOMASK 0x00FFFFFF
/*----------------------------------------------------------------------------*/
static volatile unsigned int *gpio = MAP_FAILED;
/*----------------------------------------------------------------------------*/
/** set 4K block size? */
#define MEM_BLOCK_SIZE (1<<12)
/*----------------------------------------------------------------------------*/
int gpio_init(void) {
	int gpio_fd;
	/* allows gpio_init to be called multiple times */
	if (gpio!=MAP_FAILED) return GPIO_STATUS_OK;
	/* open kernel gpiomem access */
	gpio_fd = open("/dev/gpiomem", O_RDWR|O_SYNC);
	if (gpio_fd==GPIO_STATUS_ERROR) return gpio_fd;
	/* setup memory map for gpio registers */
	gpio = (unsigned int*) mmap(0x0,MEM_BLOCK_SIZE,
		(PROT_READ | PROT_WRITE),MAP_SHARED,gpio_fd,GPIO_OFFSET);
	close(gpio_fd);
	return (gpio==MAP_FAILED)? GPIO_STATUS_ERROR : GPIO_STATUS_OK;
}
/*----------------------------------------------------------------------------*/
void gpio_free(void) {
	if (gpio!=MAP_FAILED) {
		munmap((void*)gpio,MEM_BLOCK_SIZE);
		gpio = MAP_FAILED;
	}
}
/*----------------------------------------------------------------------------*/
void gpio_config(int gpio_num, int gpio_sel) {
	unsigned int shift = (gpio_num%10)*GPIO_SELECT_BITS;
	unsigned int index = (gpio_num/10)+GPIO_FSEL;
	unsigned int mask = GPIO_SELECT << shift;
	unsigned int value = gpio_sel << shift;
	gpio[index] &= ~mask;
	gpio[index] |= value;
}
/*----------------------------------------------------------------------------*/
void gpio_set(int gpio_num) {
	gpio[GPIO_FSET+(gpio_num/32)] = 1 << (gpio_num%32);
}
/*----------------------------------------------------------------------------*/
void gpio_clr(int gpio_num) {
	gpio[GPIO_FCLR+(gpio_num/32)] = 1 << (gpio_num%32);
}
/*----------------------------------------------------------------------------*/
unsigned int gpio_read(int gpio_num) {
	return gpio[GPIO_FGET+(gpio_num/32)] & (1<<(gpio_num%32));
}
/*----------------------------------------------------------------------------*/
void gpio_toggle(int gpio_num) {
	if(gpio_read(gpio_num)) gpio_clr(gpio_num);
	else gpio_set(gpio_num);
}
/*----------------------------------------------------------------------------*/
#ifndef GPIO_PULL_WAIT
#define GPIO_PULL_WAIT 150
#endif
/*----------------------------------------------------------------------------*/
void gpio_pull(int gpio_num, int pull_dir) {
	unsigned int shift = (gpio_num%32);
	unsigned int index = (gpio_num/32)+1;
	unsigned int loop;
	gpio[GPIO_FPUD] = pull_dir & GPIO_PULL_MASK;
	for(loop=0;loop<GPIO_PULL_WAIT;loop++); /* setup time: 150 cycles? */
	gpio[GPIO_FPUD+index] = 1 << shift; /* enable ppud clock */
	for(loop=0;loop<GPIO_PULL_WAIT;loop++); /* hold time: 150 cycles? */
	gpio[GPIO_FPUD] = GPIO_PULL_NONE;
	gpio[GPIO_FPUD+index] = 0; /* disable ppud clock */
}
/*----------------------------------------------------------------------------*/
void gpio_write(int gpio_num, int value) {
	if (value) gpio_set(gpio_num);
	else gpio_clr(gpio_num);
}
/*----------------------------------------------------------------------------*/
unsigned int gpio_check(int gpio_num) {
	unsigned int shift = (gpio_num%10)*GPIO_SELECT_BITS;
	unsigned int index = (gpio_num/10)+GPIO_FSEL;
	unsigned int mask = GPIO_SELECT << shift;
	return (gpio[index]&mask) >> shift;
}
/*----------------------------------------------------------------------------*/
void gpio_init_data(int gpio_sel) {
	gpio[GPIO_DATA_DECADE] &= ~GPIO_DATA_DOMASK;
	if(gpio_sel==GPIO_OUTPUT)
		gpio[GPIO_DATA_DECADE] |= GPIO_DATA_OUTPUT;
}
/*----------------------------------------------------------------------------*/
void gpio_put_data(unsigned int data) {
	gpio[GPIO_FSET] = (data&0xff)<<GPIO_DATA_OFFSET;
	gpio[GPIO_FCLR] = (~data&0xff)<<GPIO_DATA_OFFSET;
}
/*----------------------------------------------------------------------------*/
unsigned int gpio_get_data(void) {
	return (gpio[GPIO_FGET]>>GPIO_DATA_OFFSET)&0xff;
}
/*----------------------------------------------------------------------------*/
void gpio_setevent(int gpio_num,int events) {
	unsigned int shift = (gpio_num%32);
	unsigned int index = (gpio_num/32);
	unsigned int mask = 1 << shift;
	unsigned int value = 1 << shift;
	/* clear by default, set only if requested */
	/* enable rising edge detect status */
	gpio[GPIO_EREN+index] &= ~mask;
	if(events&GPIO_EVENT_EDGER)
		gpio[GPIO_EREN+index] |= value;
	/* enable falling edge detect status */
	gpio[GPIO_EFEN+index] &= ~mask;
	if(events&GPIO_EVENT_EDGEF)
		gpio[GPIO_EFEN+index] |= value;
	/* enable high level detect status */
	gpio[GPIO_LHEN+index] &= ~mask;
	if(events&GPIO_EVENT_LVLHI)
		gpio[GPIO_LHEN+index] |= value;
	/* enable low level detect status */
	gpio[GPIO_LLEN+index] &= ~mask;
	if(events&GPIO_EVENT_LVLLO)
		gpio[GPIO_LLEN+index] |= value;
	/* enable asynchronous rising edge detect status */
	gpio[GPIO_AREN+index] &= ~mask;
	if(events&GPIO_EVENT_AEDGR)
		gpio[GPIO_AREN+index] |= value;
	/* enable asynchronous falling edge detect status */
	gpio[GPIO_AFEN+index] &= ~mask;
	if(events&GPIO_EVENT_AEDGF)
		gpio[GPIO_AFEN+index] |= value;
}
/*----------------------------------------------------------------------------*/
void gpio_rstevent(int gpio_num) {
	gpio[GPIO_EVDS+(gpio_num/32)] = (1<<(gpio_num%32));
}
/*----------------------------------------------------------------------------*/
unsigned int gpio_chkevent(int gpio_num) {
	return gpio[GPIO_EVDS+(gpio_num/32)] & (1<<(gpio_num%32));
}
/*----------------------------------------------------------------------------*/
