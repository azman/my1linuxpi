/*----------------------------------------------------------------------------*/
#include "gsm.h"
/*----------------------------------------------------------------------------*/
/* requires my1codelib project */
#include "my1uart.h"
/*----------------------------------------------------------------------------*/
#define GSM_WAIT_DELAY 2000000
/*----------------------------------------------------------------------------*/
#include <sys/time.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void timer_wait(int delay_us)
{
	struct timeval init, curr, stop;
	gettimeofday(&init,0x0);
	curr.tv_sec = delay_us/1000000;
	curr.tv_usec = delay_us%1000000;
	timeradd(&init,&curr,&stop);
	do {
		gettimeofday(&curr,0x0);
	} while (timercmp(&curr,&stop,<));
}
/*----------------------------------------------------------------------------*/
static my1uart_t* g_port;
/*----------------------------------------------------------------------------*/
void gsm_init(void* port)
{
	g_port = (my1uart_t*) port;
}
/*----------------------------------------------------------------------------*/
void gsm_command(char* message)
{
	while (*message)
	{
		uart_send_byte(g_port,(byte08_t)*message);
		message++;
	}
	uart_send_byte(g_port,(byte08_t)0x0d); /** CR only */
}
/*----------------------------------------------------------------------------*/
int gsm_checkok(char* message, int count)
{
	int ok_found = 0;
	if (count>=GSM_OK_SIZE)
		if (!strncmp(&message[count-GSM_OK_SIZE],GSM_OK,GSM_OK_SIZE))
			ok_found = 1;
	return ok_found;
}
/*----------------------------------------------------------------------------*/
int gsm_timeout(int delay)
{
	unsigned int time = 1;
	struct timeval init, curr, stop;
	gettimeofday(&init,0x0);
	curr.tv_sec = delay/1000000;
	curr.tv_usec = delay%1000000;
	timeradd(&init,&curr,&stop);
	do {
		gettimeofday(&curr,0x0);
		if (uart_incoming(g_port)) { time = 0; break; }
	} while (timercmp(&curr,&stop,<));
	return time;
}
/*----------------------------------------------------------------------------*/
int gsm_replies(char* message, int size, unsigned int *okstat)
{
	char *pstrok = GSM_OK;
	int count = 0, okay = GSM_OK_NOTFOUND;
	while (1)
	{
		if (gsm_timeout(GSM_WAIT_DELAY)) break;
		message[count] = uart_read_byte(g_port);
		count++;
		if (count==size-1) break;
		if (message[count-1]==pstrok[okay])
		{
			okay++;
			if (okay==GSM_OK_COMPLETE) break;
		}
		else if (message[count-1]==pstrok[0]) okay = GSM_OK_FOUNDNEW;
		else okay = GSM_OK_NOTFOUND;
	}
	message[count] = 0x0;
	if (okstat) *okstat = okay;
	return count;
}
/*----------------------------------------------------------------------------*/
char* gsm_trim_prefix(char* message)
{
	/* trim prefixed \r\n, some gsm module do not send these */
	char *pbuff = message;
	while (*pbuff=='\r'||*pbuff=='\n') pbuff++;
	return pbuff;
}
/*----------------------------------------------------------------------------*/
