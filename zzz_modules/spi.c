/*----------------------------------------------------------------------------*/
#include "spi.h"
#include "gpio.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
void spi_init(spi_t* pspi, int wait) {
	pspi->cenb = SPI_CENB;
	pspi->cclk = SPI_CCLK;
	pspi->mosi = SPI_MOSI;
	pspi->miso = SPI_MISO;
	gpio_init();
	gpio_config(pspi->cenb,GPIO_OUTPUT);
	gpio_config(pspi->cclk,GPIO_OUTPUT);
	gpio_config(pspi->mosi,GPIO_OUTPUT);
	gpio_config(pspi->miso,GPIO_INPUT);
	gpio_set(pspi->cenb);
	gpio_clr(pspi->cclk);
	gpio_clr(pspi->mosi);
	if (wait<1) wait = SPI_WAIT;
	pspi->spi_wait = wait;
}
/*----------------------------------------------------------------------------*/
void spi_enable(spi_t* pspi, int enab) {
	if (enab) {
		gpio_clr(pspi->cenb);
		timer_wait(pspi->spi_wait>>1);
	}
	else {
		gpio_set(pspi->cenb);
		timer_wait(pspi->spi_wait<<1);
	}
}
/*----------------------------------------------------------------------------*/
int spi_transfer(spi_t* pspi, int sdat) {
	int loop, read;
	for (loop=0,read=0;loop<8;loop++) {
		read <<= 1;
		/* msb first! */
		if (sdat&0x80) gpio_set(pspi->mosi);
		else gpio_clr(pspi->mosi);
		if (gpio_read(pspi->miso)) read |= 0x01;
		timer_wait(pspi->spi_wait);
		gpio_set(pspi->cclk);
		timer_wait(pspi->spi_wait);
		gpio_clr(pspi->cclk);
		sdat <<= 1;
	}
	return read;
}
/*----------------------------------------------------------------------------*/
