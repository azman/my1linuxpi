/*----------------------------------------------------------------------------*/
#include "frc522.h"
#include "timer.h"
/*----------------------------------------------------------------------------*/
#define FRC522_ADDRESS_MASK 0x7E
#define FRC522_READ_MASK 0x80
/*----------------------------------------------------------------------------*/
void frc522_reg_write(spi_t *pspi, int addr, int dval)  {
	spi_enable(pspi,1);
	spi_transfer(pspi,(addr<<1)&FRC522_ADDRESS_MASK);
	spi_transfer(pspi,dval);
	spi_enable(pspi,0);
}
/*----------------------------------------------------------------------------*/
int frc522_reg_read(spi_t *pspi, int addr) {
	int temp;
	spi_enable(pspi,1);
	spi_transfer(pspi,((addr<<1)&FRC522_ADDRESS_MASK)|FRC522_READ_MASK);
	temp = spi_transfer(pspi,0x00);
	spi_enable(pspi,0);
	return temp;
}
/*----------------------------------------------------------------------------*/
#define TIMER_AUTO 0x80
#define TIMER_MASK_PRESCALERH 0x0F
#define TIMER_6780KHZ_PRESCALEH 0x0D
#define TIMER_6780KHZ_PRESCALEL 0x3E
#define MODULATE_FORCE100ASK 0x40
#define MODE_TX_WAITRF 0x20
#define MODE_RESERVED0 0x10
#define MODE_POLMFIN 0x08
#define MODE_RESERVED1 0x04
#define MODE_CRC_PRESET_FFFF 0x03
#define MODE_CRC_PRESET_A671 0x02
#define MODE_CRC_PRESET_6363 0x01
#define MODE_CRC_PRESET_0000 0x00
#define CNTL_TX2RF_ENABLE 0x02
#define CNTL_TX1RF_ENABLE 0x01
#define CNTL_TXALL_ENABLE (CNTL_TX2RF_ENABLE|CNTL_TX1RF_ENABLE)
/*----------------------------------------------------------------------------*/
#define INIT_P1_TXCTRL CNTL_TX2RF_ENABLE|CNTL_TX1RF_ENABLE
/* for f=6.78MHz? - prescale(0xd3e=3390):2kHz -> t=0.5ms? */
#define INIT_PRESCALE_H (TIMER_6780KHZ_PRESCALEH&TIMER_MASK_PRESCALERH)
#define INIT_PRESCALE_L TIMER_6780KHZ_PRESCALEL
/* reload timer at 30x0.5ms=15ms? */
#define INIT_TRELOAD_H 0x00
#define INIT_TRELOAD_L 0x1e
/*----------------------------------------------------------------------------*/
#define TXRX_ERROR_FLAG0 ERROR_BUFFER_OVERFLOW|ERROR_BIT_COLLISION
#define TXRX_ERROR_FLAGS TXRX_ERROR_FLAG0|ERROR_PARITY_FAILED|ERROR_PROTOCOL
/*----------------------------------------------------------------------------*/
int frc522_init(spi_t *pspi) {
	int temp;
	/* spi should already be initialized */
	frc522_reg_write(pspi,FRC522_P0_COMMAND_REG,FRC522_SOFT_RESET);
	frc522_reg_write(pspi,FRC522_P2_TMODE_REG,TIMER_AUTO|INIT_PRESCALE_H);
	frc522_reg_write(pspi,FRC522_P2_TPRESCALER_REG,INIT_PRESCALE_L);
	frc522_reg_write(pspi,FRC522_P2_TRELOADL_REG,INIT_TRELOAD_L);
	frc522_reg_write(pspi,FRC522_P2_TRELOADH_REG,INIT_TRELOAD_H);
	frc522_reg_write(pspi,FRC522_P1_TX_ASK_REG,MODULATE_FORCE100ASK);
	/* switch on antenna */
	temp = frc522_reg_read(pspi,FRC522_P1_TX_CNTL_REG);
	frc522_reg_write(pspi,FRC522_P1_TX_CNTL_REG,temp|CNTL_TXALL_ENABLE);
	/* returns firmware version */
	return frc522_reg_read(pspi,FRC522_P3_VERSION_REG);
}
/*----------------------------------------------------------------------------*/
#define COMIEN_IRQINV 0x80
#define COMIRQ_SET1 0x80
#define COMIRQ_RXIRQ 0x20
#define COMIRQ_IDLEIRQ 0x10
#define COMIRQ_TIMERIRQ 0x01
#define COMIRQ_WAITIRQ (COMIRQ_RXIRQ|COMIRQ_IDLEIRQ)
#define FIFO_FLUSHBUFF 0x80
#define BITFRAME_STARTSEND 0x80
#define BITFRAME_TXLASTBITS 0x07
#define ERROR_BUFFER_OVERFLOW 0x10
#define ERROR_BIT_COLLISION 0x08
#define ERROR_CRC_FAILED 0x04
#define ERROR_PARITY_FAILED 0x02
#define ERROR_PROTOCOL 0x01
/*----------------------------------------------------------------------------*/
void frc522_prep_tx(spi_t *pspi) {
	int temp;
	temp = frc522_reg_read(pspi,FRC522_P0_COMIRQ_REG); /* clear all irq */
	frc522_reg_write(pspi,FRC522_P0_COMIRQ_REG,temp&~COMIRQ_SET1);
	temp = frc522_reg_read(pspi,FRC522_P0_FIFOLVL_REG); /* flush fifo buffers */
	frc522_reg_write(pspi,FRC522_P0_FIFOLVL_REG,temp|FIFO_FLUSHBUFF);
	frc522_reg_write(pspi,FRC522_P0_COMMAND_REG,FRC522_IDLE); /* idle mode */
}
/*----------------------------------------------------------------------------*/
int frc522_push_tx(spi_t *pspi) {
	unsigned int last;
	int temp;
	frc522_reg_write(pspi,FRC522_P0_COMMAND_REG,FRC522_TRANSCEIVE);
	temp = frc522_reg_read(pspi,FRC522_P0_BITFRAME_REG);
	frc522_reg_write(pspi,FRC522_P0_BITFRAME_REG,temp|BITFRAME_STARTSEND);
	/* wait... <25ms? */
	last = timer_read();
	do {
		temp = frc522_reg_read(pspi,FRC522_P0_COMIRQ_REG);
		if ((timer_read()-last)>25000) return FRC522_ERROR_TIMEOUT;
	} while (!(temp&COMIRQ_TIMERIRQ)&&!(temp&COMIRQ_WAITIRQ));
	pspi->last = timer_read()-last;
	pspi->curr = temp;
	if (frc522_reg_read(pspi,FRC522_P0_ERROR_REG)&(TXRX_ERROR_FLAGS))
		return FRC522_ERROR_TXRX;
	if (temp&COMIRQ_TIMERIRQ)
		return FRC522_ERROR_NO_TAG;
	return FRC522_OK;
}
/*----------------------------------------------------------------------------*/
int frc522_pull_rx(spi_t *pspi, unsigned char *pdat) {
	int temp, test, loop;
	temp = frc522_reg_read(pspi,FRC522_P0_FIFOLVL_REG);
	test = frc522_reg_read(pspi,FRC522_P0_CONTROL_REG)&0x07;
	if (!test) test = 8; // valid bits!
	if (temp==0) temp = 1; // minimum single read
	else if (temp > FRC522_MAX_RXSIZE) // buffer limit
		temp = FRC522_MAX_RXSIZE;
	for (loop=0;loop<temp;loop++)
		pdat[loop] = frc522_reg_read(pspi,FRC522_P0_FIFODATA_REG);
	return temp;
}
/*----------------------------------------------------------------------------*/
int frc522_scan(spi_t *pspi, unsigned char *buff, int *blen) {
	int temp, test, loop;
	/* buff size should be FRC522_MAX_RXSIZE */
	/* prepare for tx */
	frc522_reg_write(pspi,FRC522_P0_BITFRAME_REG,BITFRAME_TXLASTBITS);
	frc522_prep_tx(pspi);
	/* mifare cmd ReqA */
	frc522_reg_write(pspi,FRC522_P0_FIFODATA_REG,MF1_REQIDL);
	temp = frc522_push_tx(pspi);
	if (temp!=FRC522_OK) return temp;
	temp = frc522_pull_rx(pspi,buff);
	if (temp!=2) return FRC522_ERROR_REQ_A;
/**
CardType | ATQA (16-bits) | SAK | UID Size
Mifare Classic 1K | 00 04 | 08 | 4 Bytes
Mifare Classic 4K | 00 02 | 18 | 4 Bytes
Mifare Ultralight | 00 44 | 00 | 7 Bytes
- note: SAK obtained in SELECT command?
**/
	/* park this here for now - note that these are little-endian! */
	buff[FRC522_MAX_RXSIZE-2] = buff[0];
	buff[FRC522_MAX_RXSIZE-1] = buff[1];
	/* prepare for tx */
	frc522_reg_write(pspi,FRC522_P0_BITFRAME_REG,0x00);
	frc522_prep_tx(pspi);
	/* mifare cmd CascadeLevel1? */
	frc522_reg_write(pspi,FRC522_P0_FIFODATA_REG,MF1_ANTICOLL);
	frc522_reg_write(pspi,FRC522_P0_FIFODATA_REG,0x20);
	temp = frc522_push_tx(pspi);
	if (temp!=FRC522_OK) return temp;
	temp = frc522_pull_rx(pspi,buff);
	/* checksum */
	test = 0x00; temp--; /* exclude checksum! */
	for (loop=0;loop<temp;loop++)
		test ^= buff[loop];
	if (test != buff[loop])
		return FRC522_ERROR_CHECKSUM;
	if (blen) *blen = temp;
	return FRC522_OK;
}
/*----------------------------------------------------------------------------*/
