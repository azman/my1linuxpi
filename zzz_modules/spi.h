/*----------------------------------------------------------------------------*/
#ifndef __MY1SPI_H__
#define __MY1SPI_H__
/*----------------------------------------------------------------------------*/
/**
 * - bit-bang spi lib
**/
/*----------------------------------------------------------------------------*/
typedef struct _spi_t {
	int mosi, miso, cenb, cclk; /* gpio_nums */
	unsigned int spi_wait, spi_temp;
	unsigned int last, curr;
} spi_t;
/*----------------------------------------------------------------------------*/
#define SPI_MOSI 5
#define SPI_MISO 6
#define SPI_CENB 13
#define SPI_CCLK 19
/*----------------------------------------------------------------------------*/
#define SPI_WAIT 5
/*----------------------------------------------------------------------------*/
void spi_init(spi_t* pspi, int wait);
void spi_enable(spi_t* pspi, int enab);
int spi_transfer(spi_t* pspi, int sdat);
/*----------------------------------------------------------------------------*/
#endif /* __MY1SPI_H__ */
/*----------------------------------------------------------------------------*/
