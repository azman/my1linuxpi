/*----------------------------------------------------------------------------*/
#ifndef __MY1TIMER_H__
#define __MY1TIMER_H__
/*----------------------------------------------------------------------------*/
typedef int (*timeout_check)(void);
/*----------------------------------------------------------------------------*/
int timer_timeout(unsigned int delay_us,timeout_check checker);
void timer_wait(unsigned int delay_us);
/*----------------------------------------------------------------------------*/
/* 32-bit 1us-count */
unsigned int timer_read(void);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
