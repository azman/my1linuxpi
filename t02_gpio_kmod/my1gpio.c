/*----------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/module.h>
/*----------------------------------------------------------------------------*/
/* using raspbian gpio kernel module */
#include <linux/gpio.h>
/*----------------------------------------------------------------------------*/
#define MY1MODULE_NAME "my1gpio"
#define MY1DEVICE_NAME "squarewave"
/*----------------------------------------------------------------------------*/
/* must have this to use exported symbols */
MODULE_LICENSE("GPL");
/*----------------------------------------------------------------------------*/
#include <linux/hrtimer.h>
#include <linux/ktime.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_GPIO 26
#define GPIO_OUTPUT 0
#define GPIO_INPUT 1
/*----------------------------------------------------------------------------*/
static struct hrtimer squarewave_timer;
static unsigned long squarewave_period = 10000; /* in ns! */
static unsigned int squarewave_gpio = DEFAULT_GPIO;
/*----------------------------------------------------------------------------*/
static enum hrtimer_restart squarewave_timer_handler(struct hrtimer* timer)
{
	ktime_t curr, step;
	gpio_set_value(squarewave_gpio,!gpio_get_value(squarewave_gpio));
	curr = ktime_get();
	step = ktime_set(0,squarewave_period);
	hrtimer_forward(timer,curr,step);
	return HRTIMER_RESTART;
}
/*----------------------------------------------------------------------------*/
static ssize_t set_period_callback(struct device* dev,
	struct device_attribute* attr, const char* buf, size_t count)
{
	long cycle_period = 0;
	int result = kstrtol(buf, 10, &cycle_period);
	/** sucess: 0 ; error: -ERANGE , -EINVAL */
	if (result) return result;
	/** minimum period is ? */
	if (!cycle_period) return -EINVAL;
	/** assign period */
	squarewave_period = (int)cycle_period;
	/** return raw input length? */
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t get_period_callback(struct device* dev,
	struct device_attribute* attr, char* buf)
{
	/* get squarewave_period value */
	return scnprintf(buf, PAGE_SIZE, "%lu\n", squarewave_period);
}
/*----------------------------------------------------------------------------*/
static ssize_t set_gpio_num_callback(struct device* dev,
	struct device_attribute* attr, const char* buf, size_t count)
{
	long gpio_num = 0;
	int result = kstrtol(buf, 10, &gpio_num);
	/** sucess: 0 ; error: -ERANGE , -EINVAL */
	if (result) return result;
	/** validate gpio_num */
	if (!gpio_is_valid(gpio_num)) return -EINVAL;
	/** setup gpio_num and reassign squarewave_gpio, if required */
	if (gpio_num!=squarewave_gpio)
	{
		/* setup gpio_num (new gpio) */
		result = gpio_request(gpio_num,MY1MODULE_NAME);
		if (result) return -EINVAL;
		gpio_direction_output(gpio_num,GPIO_OUTPUT); /* as output */
		gpio_export(gpio_num,false); /* export and no direction change */
		/* release gpio */
		gpio_unexport(squarewave_gpio);
		gpio_free(squarewave_gpio);
		squarewave_gpio = gpio_num;
	}
	/** return raw input length? */
	return count;
}
/*----------------------------------------------------------------------------*/
static ssize_t get_gpio_num_callback(struct device* dev,
	struct device_attribute* attr, char* buf)
{
	/* get squarewave_period value */
	return scnprintf(buf, PAGE_SIZE, "%u\n", squarewave_gpio);
}
/*----------------------------------------------------------------------------*/
#include <linux/device.h>
/*----------------------------------------------------------------------------*/
static DEVICE_ATTR(period, S_IWUSR | S_IWGRP | S_IRUGO,
	get_period_callback, set_period_callback);
static DEVICE_ATTR(gpio_num, S_IWUSR | S_IWGRP | S_IRUSR | S_IRGRP,
	get_gpio_num_callback, set_gpio_num_callback);
/*----------------------------------------------------------------------------*/
static struct class *s_pDeviceClass;
static struct device *s_pDeviceObject;
/*----------------------------------------------------------------------------*/
#include <linux/timer.h>
/*----------------------------------------------------------------------------*/
static int __init my1gpio_init(void)
{
	int result;
	ktime_t ktime;
	/* hello! */
	printk(KERN_INFO "Hello, %s.\n",MY1MODULE_NAME);
	/* create sysfs class */
	s_pDeviceClass = class_create(THIS_MODULE,MY1MODULE_NAME);
	BUG_ON(IS_ERR(s_pDeviceClass));
	/* create sysfs device */
	s_pDeviceObject = device_create(s_pDeviceClass,NULL,0,NULL,MY1DEVICE_NAME);
	BUG_ON(IS_ERR(s_pDeviceObject));
	/* create sysfs file interface */
	result = device_create_file(s_pDeviceObject, &dev_attr_period);
	BUG_ON(result < 0);
	result = device_create_file(s_pDeviceObject, &dev_attr_gpio_num);
	BUG_ON(result < 0);
	/* configure gpio */
	gpio_request(squarewave_gpio,MY1MODULE_NAME);
	gpio_direction_output(squarewave_gpio,GPIO_OUTPUT); /* as output */
	gpio_export(squarewave_gpio,false); /* export and no direction change */
	/* setup high resolution timer */
	ktime = ktime_set(0,squarewave_period);
	hrtimer_init(&squarewave_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	squarewave_timer.function = &squarewave_timer_handler;
	hrtimer_start(&squarewave_timer,ktime,HRTIMER_MODE_REL);
	return 0;
}
/*----------------------------------------------------------------------------*/
static void __exit my1gpio_exit(void)
{
	/* remove sysfs file interface */
	device_remove_file(s_pDeviceObject, &dev_attr_gpio_num);
	device_remove_file(s_pDeviceObject, &dev_attr_period);
	/* remove sysfs device */
	device_destroy(s_pDeviceClass, 0);
	/* remove sysfs class */
	class_destroy(s_pDeviceClass);
	/* cancel hrtimer - blocking function */
	hrtimer_cancel(&squarewave_timer);
	/* release gpio */
	gpio_unexport(squarewave_gpio);
	gpio_free(squarewave_gpio);
	/* bye! */
	printk(KERN_INFO "Goodbye, %s.\n",MY1MODULE_NAME);
}
/*----------------------------------------------------------------------------*/
module_init(my1gpio_init);
module_exit(my1gpio_exit);
/*----------------------------------------------------------------------------*/
