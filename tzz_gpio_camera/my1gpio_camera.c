/*----------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/module.h>
/*----------------------------------------------------------------------------*/
/* using raspbian gpio kernel module */
#include <linux/gpio.h>
/*----------------------------------------------------------------------------*/
#define MY1MODULE_NAME "my1gpio_camera"
#define MY1SYSCLS_NAME "my1gpio"
#define MY1SYSDEV_NAME "camera"
/*----------------------------------------------------------------------------*/
/* must have this to use exported symbols */
MODULE_LICENSE("GPL");
/*----------------------------------------------------------------------------*/
#define GPIO_HREF 5
#define GPIO_VSYN 6
#define GPIO_PCLK 13
#define BUFF_SIZE (352*288)
/*----------------------------------------------------------------------------*/
/* last pixel timestamp */
static unsigned long pixels[BUFF_SIZE];
static int pixrd, pixwr;
static int pixdo, pixof;
static int phref, pvsyn;
static int hstep, wstep, pstep, tstep, dflag;
/*----------------------------------------------------------------------------*/
static struct gpio camsig[] =
{
	{ GPIO_HREF,GPIOF_IN,"HREF" },
	{ GPIO_VSYN,GPIOF_IN,"VSYN" },
	{ GPIO_PCLK,GPIOF_IN,"PCLK" },
};
/*----------------------------------------------------------------------------*/
static int camsig_irqs[] = { -1,-1,-1 };
/*----------------------------------------------------------------------------*/
static irq_return_t my1gpio_camera_href_handler(unsigned int irq,
		void *dev_id, struct pt_regs *regs)
{
	if (!gpio_read(GPIO_HREF))
	{
		hstep++;
		tstep = wstep;
		wstep = 0;
	}
}
/*----------------------------------------------------------------------------*/
static irq_return_t my1gpio_camera_vsyn_handler(unsigned int irq,
		void *dev_id, struct pt_regs *regs)
{
	if (!gpio_read(GPIO_VSYN))
	{
		if (dflag)
		{
			printk(KERN_INFO "[%s] Pixel:%d (H:%d)\n",
				MY1MODULE_NAME,pstep,hstep);
			dflag = 0;
		}
		pstep = 0; wstep = 0; hstep = 0;
	}
}
/*----------------------------------------------------------------------------*/
static irq_return_t my1gpio_camera_pclk_handler(unsigned int irq,
		void *dev_id, struct pt_regs *regs)
{
	if (gpio_read(GPIO_HREF))
	{
		wstep++; pstep++;
		/**pixels[pixdo++] = gpio_data*/
	}
}
/*----------------------------------------------------------------------------*/
static ssize_t pcount_show(struct kobject *kobj, struct kobj_attribute *attr,
		char *buf)
{
	return sprintf(buf, "%d\n", numberPresses);
}
/*----------------------------------------------------------------------------*/
static int __init my1gpio_camera_init(void)
{
	/* hello! */
	printk(KERN_INFO "Hello, %s.\n",MY1MODULE_NAME);
	return 0;
}
/*----------------------------------------------------------------------------*/
static void __exit my1gpio_camera_exit(void)
{
	/* bye! */
	printk(KERN_INFO "Goodbye, %s.\n",MY1MODULE_NAME);
}
/*----------------------------------------------------------------------------*/
module_init(my1gpio_init);
module_exit(my1gpio_exit);
/*----------------------------------------------------------------------------*/
