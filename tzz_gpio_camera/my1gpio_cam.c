/*----------------------------------------------------------------------------*/
#include "gpio.h"
#include "i2c.h"
#include "timer.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
/*----------------------------------------------------------------------------*/
/* checked using i2cdetect -y 1 */
#define CAM_I2C_ADDR 0x60
/*----------------------------------------------------------------------------*/
/* COMA OPTIONS - default 0x24 */
#define CAM_COMA_ADDR_ 0x12
#define CAM_SOFT_RESET 0x80
#define CAM_MIRROR_IMG 0x40
#define CAM_ENABLE_AGC 0x20
#define CAM_OUTPUT_D8B 0x10
#define CAM_ENABLE_RGB 0x08
#define CAM_ENABLE_AWB 0x04
/*----------------------------------------------------------------------------*/
/* COMH OPTIONS - default 0x01 */
#define CAM_COMH_ADDR_ 0x28
#define CAM_RGB_1_LINE 0x80
#define CAM_BITRESERVE 0x01
/*----------------------------------------------------------------------------*/
/* CLKRC OPTIONS - default 0x00 */
#define CAM_CLKRC_ADDR 0x11
#define CAM_PRESCALER_MASK 0x3F
#define CAM_PRESCALER_DEF 0x3A
/*----------------------------------------------------------------------------*/
#define GPIO_HREF 5
#define GPIO_VSYN 6
#define GPIO_PCLK 13
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	i2c_t that;
	int loop, test, addr = CAM_I2C_ADDR, data, regs;
#if 0
	int cnth = 0, cntw = 0, cntp = 0, tmpw;
#endif
	int ckps = CAM_PRESCALER_DEF;
	char *pname = basename(argv[0]);
	/* always show program name/info  */
	printf("\n%s - Raspberry Pi Camera Tool for C3038\n\n",pname);
	/* get optional params */
	for (loop=1;loop<argc;loop++)
	{
		if (!strncmp(argv[loop],"--addr",6))
		{
			if (++loop>=argc)
			{
				printf("No value given for address! Using default!\n");
			}
			else
			{
				sscanf(argv[loop],"%x",&test);
				/* based on i2cdetect range? */
				if (test<0x03||test>0x77)
				{
					printf("Invalid address (0x%02x)! Using default!\n",test);
				}
				else
				{
					addr = test;
				}
			}
		}
		else if (!strncmp(argv[loop],"--scale",7))
		{
			if (++loop>=argc)
			{
				printf("No value given for prescaler! Using default!\n");
			}
			else
			{
				sscanf(argv[loop],"%x",&test);
				/* based on datasheet */
				if (test<0x00||test>CAM_PRESCALER_MASK)
				{
					printf("Invalid prescale (0x%02x)! Using default!\n",test);
				}
				else
				{
					ckps = test;
				}
			}
		}
	}
	/* initialize gpio library */
	printf("Initializing GPIO... ");
	//fflush(stdout);
	if (gpio_init()==GPIO_STATUS_ERROR)
	{
		printf("Cannot initialize GPIO library! Aborting!\n");
		exit(1);
	}
	printf("done.\n");
	//fflush(stdout);
	/* initialize i2c library */
	i2c_init(&that,2,3);
	/* reset camera */
	printf("Resetting camera...");
	i2c_putb(&that,addr,CAM_COMA_ADDR_,CAM_SOFT_RESET);
	printf("done.\n");
	timer_wait(500000);
	//fflush(stdout);
	/* read camera regs */
	printf("Reading CLKRC (0x%02X) register: ",CAM_CLKRC_ADDR);
	data = i2c_getb(&that,addr,CAM_CLKRC_ADDR);
	printf("0x%02X\n",(data&0xff));
	//fflush(stdout);
	printf("Reading COMA (0x%02X) register: ",CAM_COMA_ADDR_);
	data = i2c_getb(&that,addr,CAM_COMA_ADDR_);
	printf("0x%02X\n",(data&0xff));
	//fflush(stdout);
	printf("Reading COMH (0x%02X) register: ",CAM_COMH_ADDR_);
	data = i2c_getb(&that,addr,CAM_COMH_ADDR_);
	printf("0x%02X\n",(data&0xff));
	//fflush(stdout);
	/* configure camera  */
	data = CAM_ENABLE_AGC | CAM_ENABLE_AWB | CAM_ENABLE_RGB;
	regs = CAM_COMA_ADDR_;
	printf("Setting rgb mode (0x%02X=>[0x%02X]): ",data,regs);
	i2c_putb(&that,addr,regs,data);
	printf("done.\n");
	//fflush(stdout);
	data = CAM_RGB_1_LINE | CAM_BITRESERVE;
	regs = CAM_COMH_ADDR_;
	printf("Setting 1-line rgb mode (0x%02X=>[0x%02X]): ",data,regs);
	i2c_putb(&that,addr,regs,data);
	printf("done.\n");
	//fflush(stdout);
	data = ckps & CAM_PRESCALER_MASK;
	regs = CAM_CLKRC_ADDR;
	printf("Setting prescaler (0x%02X=>[0x%02X]): ",data,regs);
	i2c_putb(&that,addr,regs,data);
	printf("done.\n");
	//fflush(stdout);
	/* read camera regs */
	printf("Reading CLKRC (0x%02X) register: ",CAM_CLKRC_ADDR);
	data = i2c_getb(&that,addr,CAM_CLKRC_ADDR);
	printf("0x%02X\n",(data&0xff));
	//fflush(stdout);
	printf("Reading COMA (0x%02X) register: ",CAM_COMA_ADDR_);
	data = i2c_getb(&that,addr,CAM_COMA_ADDR_);
	printf("0x%02X\n",(data&0xff));
	//fflush(stdout);
	printf("Reading COMH (0x%02X) register: ",CAM_COMH_ADDR_);
	data = i2c_getb(&that,addr,CAM_COMH_ADDR_);
	printf("0x%02X\n",(data&0xff));
	//fflush(stdout);
#if 0
	printf("Configuring GPIO for camera... ");
	//fflush(stdout);
	/* control gpio */
	gpio_config(GPIO_HREF,GPIO_INPUT); /* href */
	gpio_config(GPIO_VSYN,GPIO_INPUT); /* vsyn */
	gpio_config(GPIO_PCLK,GPIO_INPUT); /* pclk */
	/* set edge detection on href */
	gpio_setevent(GPIO_HREF,GPIO_EVENT_AEDGR|GPIO_EVENT_AEDGF);
	gpio_rstevent(GPIO_HREF);
	/* set edge detection on vsyn */
	gpio_setevent(GPIO_VSYN,GPIO_EVENT_AEDGR);
	gpio_rstevent(GPIO_VSYN);
	/* set edge detection on pclk */
	gpio_setevent(GPIO_PCLK,GPIO_EVENT_AEDGR);
	gpio_rstevent(GPIO_PCLK);
	printf("done.\n");
	//fflush(stdout);
	/* main loop */
	while (1)
	{
		if(get_keyhit()==KEY_ESCAPE)
		{
			printf("Exiting");
			//fflush(stdout);
			break;
		}
		if (gpio_chkevent(GPIO_VSYN))
		{
			printf("Counts: %d (%d x %d)\n",cntp,tmpw,cnth);
			//fflush(stdout);
			cntp = 0; cntw = 0; cnth = 0;
			gpio_rstevent(GPIO_VSYN);
		}
		if (gpio_chkevent(GPIO_HREF))
		{
			if (!gpio_read(GPIO_HREF))
			{
				cnth++;
				tmpw = cntw;
				cntw = 0;
			}
			gpio_rstevent(GPIO_HREF);
		}
		if (gpio_chkevent(GPIO_PCLK))
		{
			if (gpio_read(GPIO_HREF))
			{
				cntw++; cntp++;
			}
			gpio_rstevent(GPIO_PCLK);
		}
	}
	printf("... ");
	/* cleanup */
	gpio_setevent(GPIO_HREF,0x0);
	gpio_rstevent(GPIO_HREF);
	gpio_setevent(GPIO_VSYN,0x0);
	gpio_rstevent(GPIO_VSYN);
	gpio_setevent(GPIO_PCLK,0x0);
	gpio_rstevent(GPIO_PCLK);
#else
	printf("We are ");
#endif
	gpio_free();
	printf("done!\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
